clc
clear all
close all
R=1000;
C=10^(-6);
Li=1;
L=[1];
M=[(Li*C) (R*C) 1];
sys=tf(L,M)
figure(1)
freqs(L,M)
figure(2)
impulse(L,M)
figure(3)
step(L,M)
figure(4)
iopzplot(sys)
[z,p,k]=tf2zp(L,M)
