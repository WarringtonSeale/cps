

clc
clear all
close all
f1=100;
fs=1000;
t=0:(1/fs):1.3;
Ts=1/fs;
A=1;
y=A*sin(2*pi*f1*t);
N=1024;
fft_moc=fft(y(1:N));
moc_wid=fft_moc.*conj(fft_moc)/N;
f=fs*(0:N/2-1)/N;
figure(1)
plot(f,moc_wid(1:N/2));
xlabel('Czestotliwosc [Hz]');ylabel('Moc widmowa')
