clc
clear all
close all
A=1;
f=10;
fs=1000;
t=0:(1/fs):0.35;
n=[7,30,99];
w=2*pi*f;
%1 prostokatny bipolarny
C=4*A/pi;
for j=1:3
y=0;
for i=1:2:2*n(:,j)
y=y+((1/i)*sin(i*w*t));
end
y=y*C;
Y(j,:)=y;
N=length(y);
NN(j,:)=N;
fft_moc=fft(y(1:N));
moc_wid=fft_moc.*conj(fft_moc)/N;
widmo=sqrt(fft_moc.*conj(fft_moc))/N;
f=fs*(0:N/2-1)/N;
F(j,:)=f;
M(j,:)=moc_wid;
W(j,:)=widmo;
end
figure(1)
subplot(3,1,1)
plot(t,Y(1,:))
title('Sygnal prostokatny bipolarny dla N=7');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,2)
plot(t,Y(2,:))
title('Sygnal prostokatny bipolarny dla N=30');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,3)
plot(t,Y(3,:))
title('Sygnal prostokatny bipolarny dla N=99');
xlabel('czas [s]');
ylabel('A');
figure(2)
subplot(3,1,1)
plot(F(1,:),M(1,(1:length(Y(1,:))/2)))
title('Sygnal prostokatny bipolarny dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,2)
plot(F(2,:),M(2,(1:length(Y(2,:))/2)))
title('Sygnal prostokatny bipolarny dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,3)
plot(F(3,:),M(3,(1:length(Y(3,:))/2)))
title('Sygnal prostokatny bipolarny dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
figure(3)
subplot(3,1,1)
plot(F(1,:),W(1,(1:length(Y(1,:))/2)))
title('Sygnal prostokatny bipolarny dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,2)
plot(F(2,:),W(2,(1:length(Y(2,:))/2)))
title('Sygnal prostokatny bipolarny dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,3)
plot(F(3,:),W(3,(1:length(Y(3,:))/2)))
title('Sygnal prostokatny bipolarny dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
%2 prostokatny unipolarny wypelnienie 1/2
C1=A/2;
C2=2*(A/pi);
for j=1:3
x=0;
for i=1:4:2*n(:,j)
x=x+((1/i)*cos(i*w*t));
end
for i=3:4:2*n(:,j)
x=x-((1/i)*cos(i*w*t));
end
x=(x*C2)+C1;
X(j,:)=x;
N=length(x);
NN(j,:)=N;
fft_moc=fft(x(1:N));
moc_wid=fft_moc.*conj(fft_moc)/N;
widmo=sqrt(fft_moc.*conj(fft_moc))/N;
f=fs*(0:N/2-1)/N;
F(j,:)=f;
M(j,:)=moc_wid;
W(j,:)=widmo;
end
figure(4)
subplot(3,1,1)
plot(t,X(1,:))
title('Sygnal prostokatny unipolarny z wypelnienien 50% dla N=7');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,2)
plot(t,X(2,:))
title('Sygnal prostokatny unipolarny z wypelnienien 50% dla N=30');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,3)
plot(t,X(3,:))
title('Sygnal prostokatny unipolarny z wypelnienien 50% dla N=99');
xlabel('czas [s]');
ylabel('A');
figure(5)
subplot(3,1,1)
plot(F(1,:),M(1,(1:length(X(1,:))/2)))
title('Sygnal prostokatny unipolarny z wypelnieniem 50% dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,2)
plot(F(2,:),M(2,(1:length(X(2,:))/2)))
title('Sygnal prostokatny unipolarny z wypelnieniem 50% dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,3)
plot(F(3,:),M(3,(1:length(X(3,:))/2)))
title('Sygnal prostokatny unipolarny z wypelnieniem 50% dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
figure(6)
subplot(3,1,1)
plot(F(1,:),W(1,(1:length(X(1,:))/2)))
title('Sygnal prostokatny unipolarny z wypelnieniem 50% dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,2)
plot(F(2,:),W(2,(1:length(X(2,:))/2)))
title('Sygnal prostokatny unipolarny z wypelnieniem 50% dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,3)
plot(F(3,:),W(3,(1:length(X(3,:))/2)))
title('Sygnal prostokatny unipolarny z wypelnieniem 50% dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
%3 prostokatny unipolarny wypelnienie dowolne
f=5;
w=2*pi*f;
T=1/f;
tau=0.1;
for j=1:3
ax=0;
for i=1:1:n(:,j)
ax=ax+sin(pi*i*tau/T)*cos(i*w*t)/(pi*i*tau/T);
end
ax=A*tau/T+2*A*tau*ax/T;
AX(:,j)=ax;
N=length(ax);
NN(j,:)=N;
fft_moc=fft(ax(1:N));
moc_wid=fft_moc.*conj(fft_moc)/N;
widmo=sqrt(fft_moc.*conj(fft_moc))/N;
f=fs*(0:N/2-1)/N;
F(j,:)=f;
M(j,:)=moc_wid;
W(j,:)=widmo;
end
figure(25)
subplot(3,1,1)
plot(t,AX(:,1))
title('Sygnal prostokatny unipolarny z wypelnienien dowolnym dla N=7');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,2)
plot(t,AX(:,2))
title('Sygnal prostokatny unipolarny z wypelnienien dowolnym dla N=30');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,3)
plot(t,AX(:,3))
title('Sygnal prostokatny unipolarny z wypelnienien dowolnym dla N=99');
xlabel('czas [s]');
ylabel('A');
figure(26)
subplot(3,1,1)
plot(F(1,:),M(1,(1:length(AX(1,:))/2)))
title('Sygnal prostokatny unipolarny z wypelnieniem dowolnym dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,2)
plot(F(2,:),M(2,(1:length(AX(2,:))/2)))
title('Sygnal prostokatny unipolarny z wypelnieniem dowolnym dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,3)
plot(F(3,:),M(3,(1:length(AX(3,:))/2)))
title('Sygnal prostokatny unipolarny z wypelnieniem dowolnym dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
figure(27)
subplot(3,1,1)
plot(F(1,:),W(1,(1:length(AX(1,:))/2)))
title('Sygnal prostokatny unipolarny z wypelnieniem dowolnym dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,2)
plot(F(2,:),W(2,(1:length(AX(2,:))/2)))
title('Sygnal prostokatny unipolarny z wypelnieniem dowolnym dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,3)
plot(F(3,:),W(3,(1:length(AX(3,:))/2)))
title('Sygnal prostokatny unipolarny z wypelnieniem dowolnym dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
%4 trojkatny bipolarny1
C_5=8*A/(pi*pi);
for j=1:3
yy=0;
for i=1:4:2*n(:,j)
yy=yy+(1/(i^2))*sin(i*w*t);
end
for i=3:4:2*n(:,j)
yy=yy-(1/(i^2))*sin(i*w*t);
end
YY(j,:)=yy*C_5;
N=length(yy);
NN(j,:)=N;
fft_moc=fft(yy(1:N));
moc_wid=fft_moc.*conj(fft_moc)/N;
widmo=sqrt(fft_moc.*conj(fft_moc))/N;
f=fs*(0:N/2-1)/N;
F(j,:)=f;
M(j,:)=moc_wid;
W(j,:)=widmo;
end
figure(7)
subplot(3,1,1)
plot(t,YY(1,:))
title('Sygnal trojkatny bipolarny 1 dla N=7');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,2)
plot(t,YY(2,:))
title('Sygnal trojkatny bipolarny 1 dla N=30');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,3)
plot(t,YY(3,:))
title('Sygnal trojkatny bipolarny 1 dla N=99');
xlabel('czas [s]');
ylabel('A');
figure(8)
subplot(3,1,1)
plot(F(1,:),M(1,(1:length(X(1,:))/2)))
title('Sygnal trojkatny bipolarny 1 dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,2)
plot(F(2,:),M(2,(1:length(X(2,:))/2)))
title('Sygnal trojkatny bipolarny 1 dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,3)
plot(F(3,:),M(3,(1:length(X(3,:))/2)))
title('Sygnal trojkatny bipolarny 1 dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
figure(9)
subplot(3,1,1)
plot(F(1,:),W(1,(1:length(X(1,:))/2)))
title('Sygnal trojkatny bipolarny 1 dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,2)
plot(F(2,:),W(2,(1:length(X(2,:))/2)))
title('Sygnal trojkatny bipolarny 1 dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,3)
plot(F(3,:),W(3,(1:length(X(3,:))/2)))
title('Sygnal trojkatny bipolarny 1 dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
%5 trojkatny bipolarny2-piloksztaltny
C_6=2*A/(pi);
for j=1:3
z=0;
for i=1:2:n(:,j)
z=z+(1/(i))*sin(i*w*t);
end
for i=2:2:n(:,j)
z=z-(1/(i))*sin(i*w*t);
end
Z(j,:)=z*C_6;
N=length(z);
NN(j,:)=N;
fft_moc=fft(z(1:N));
moc_wid=fft_moc.*conj(fft_moc)/N;
widmo=sqrt(fft_moc.*conj(fft_moc))/N;
f=fs*(0:N/2-1)/N;
F(j,:)=f;
M(j,:)=moc_wid;
W(j,:)=widmo;
end
figure(10)
subplot(3,1,1)
plot(t,Z(1,:))
title('Sygnal trojkatny bipolarny 2 dla N=7');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,2)
plot(t,Z(2,:))
title('Sygnal trojkatny bipolarny 2 dla N=30');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,3)
plot(t,Z(3,:))
title('Sygnal trojkatny bipolarny 2 dla N=99');
xlabel('czas [s]');
ylabel('A');
figure(11)
subplot(3,1,1)
plot(F(1,:),M(1,(1:length(Z(1,:))/2)))
title('Sygnal trojkatny bipolarny 2 dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,2)
plot(F(2,:),M(2,(1:length(Z(2,:))/2)))
title('Sygnal trojkatny bipolarny 2 dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,3)
plot(F(3,:),M(3,(1:length(Z(3,:))/2)))
title('Sygnal trojkatny bipolarny 2 dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
figure(12)
subplot(3,1,1)
plot(F(1,:),W(1,(1:length(Z(1,:))/2)))
title('Sygnal trojkatny bipolarny 2 dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,2)
plot(F(2,:),W(2,(1:length(Z(2,:))/2)))
title('Sygnal trojkatny bipolarny 2 dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,3)
plot(F(3,:),W(3,(1:length(Z(3,:))/2)))
title('Sygnal trojkatny bipolarny 2 dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
%6 trojkatny unipolarny1
C7=-4*A/(pi*pi);
C8=A/2;
for j=1:3
zz=0;
for i=0:n(:,j)
zz=zz+(1/((2*i+1)^2))*cos((2*i+1)*w*t);
end
ZZ(j,:)=(zz*C7)+C8;
N=length(zz);
NN(j,:)=N;
fft_moc=fft(zz(1:N));
moc_wid=fft_moc.*conj(fft_moc)/N;
widmo=sqrt(fft_moc.*conj(fft_moc))/N;
f=fs*(0:N/2-1)/N;
F(j,:)=f;
M(j,:)=moc_wid;
W(j,:)=widmo;
end
figure(13)
subplot(3,1,1)
plot(t,ZZ(1,:))
title('Sygnal trojkatny unipolarny 1 dla N=7');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,2)
plot(t,ZZ(2,:))
title('Sygnal trojkatny unipolarny 1 dla N=30');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,3)
plot(t,ZZ(3,:))
title('Sygnal trojkatny unipolarny 1 dla N=99');
xlabel('czas [s]');
ylabel('A');
figure(14)
subplot(3,1,1)
plot(F(1,:),M(1,(1:length(ZZ(1,:))/2)))
title('Sygnal trojkatny unipolarny 1 dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,2)
plot(F(2,:),M(2,(1:length(ZZ(2,:))/2)))
title('Sygnal trojkatny unipolarny 1 dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,3)
plot(F(3,:),M(3,(1:length(ZZ(3,:))/2)))
title('Sygnal trojkatny unipolarny 1 dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
figure(15)
subplot(3,1,1)
plot(F(1,:),W(1,(1:length(ZZ(1,:))/2)))
title('Sygnal trojkatny unipolarny 1 dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,2)
plot(F(2,:),W(2,(1:length(ZZ(2,:))/2)))
title('Sygnal trojkatny unipolarny 1 dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,3)
plot(F(3,:),W(3,(1:length(ZZ(3,:))/2)))
title('Sygnal trojkatny unipolarny 1 dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
%7 trojkatny unipolarny2-piloksztaltny
C9=-A/(pi);
C10=A/2;
for j=1:3
xxx=0;
for i=1:n(:,j)
xxx=xxx+(1/i)*sin(i*w*t);
end
XXX(j,:)=(xxx*C9)+C10;
N=length(xxx);
NN(j,:)=N;
fft_moc=fft(xxx(1:N));
moc_wid=fft_moc.*conj(fft_moc)/N;
widmo=sqrt(fft_moc.*conj(fft_moc))/N;
f=fs*(0:N/2-1)/N;
F(j,:)=f;
M(j,:)=moc_wid;
W(j,:)=widmo;
end
figure(16)
subplot(3,1,1)
plot(t,XXX(1,:))
title('Sygnal trojkatny unipolarny 2 dla N=7');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,2)
plot(t,XXX(2,:))
title('Sygnal trojkatny unipolarny 2 dla N=30');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,3)
plot(t,XXX(3,:))
title('Sygnal trojkatny unipolarny 2 dla N=99');
xlabel('czas [s]');
ylabel('A');
figure(17)
subplot(3,1,1)
plot(F(1,:),M(1,(1:length(XXX(1,:))/2)))
title('Sygnal trojkatny unipolarny 2 dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,2)
plot(F(2,:),M(2,(1:length(XXX(2,:))/2)))
title('Sygnal trojkatny unipolarny 2 dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,3)
plot(F(3,:),M(3,(1:length(XXX(3,:))/2)))
title('Sygnal trojkatny unipolarny 2 dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
figure(18)
subplot(3,1,1)
plot(F(1,:),W(1,(1:length(XXX(1,:))/2)))
title('Sygnal trojkatny unipolarny 2 dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,2)
plot(F(2,:),W(2,(1:length(XXX(2,:))/2)))
title('Sygnal trojkatny unipolarny 2 dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,3)
plot(F(3,:),W(3,(1:length(XXX(3,:))/2)))
title('Sygnal trojkatny unipolarny 2 dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
%8 sinusoidalny wyprostowany dwupolowkowy
C11=2*A/(pi);
C12=-4*A/(pi);
for j=1:3
yyy=0;
for i=1:n(:,j)
yyy=yyy+(1/(4*i*i-1))*cos(2*i*w*t);
end
YYY(j,:)=(yyy*C12)+C11;
N=length(yyy);
NN(j,:)=N;
fft_moc=fft(yyy(1:N));
moc_wid=fft_moc.*conj(fft_moc)/N;
widmo=sqrt(fft_moc.*conj(fft_moc))/N;
f=fs*(0:N/2-1)/N;
F(j,:)=f;
M(j,:)=moc_wid;
W(j,:)=widmo;
end
figure(19)
subplot(3,1,1)
plot(t,YYY(1,:))
title('Sygnal sinusoidalny wyprostowany dwupolowkowy dla N=7');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,2)
plot(t,YYY(2,:))
title('Sygnal sinusoidalny wyprostowany dwupolowkowy dla N=30');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,3)
plot(t,YYY(3,:))
title('Sygnal sinusoidalny wyprostowany dwupolowkowy dla N=99');
xlabel('czas [s]');
ylabel('A');
figure(20)
subplot(3,1,1)
plot(F(1,:),M(1,(1:length(YYY(1,:))/2)))
title('Sygnal sinusoidalny wyprostowany dwupolowkowy dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,2)
plot(F(2,:),M(2,(1:length(YYY(2,:))/2)))
title('Sygnal sinusoidalny wyprostowany dwupolowkowy dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,3)
plot(F(3,:),M(3,(1:length(YYY(3,:))/2)))
title('Sygnal sinusoidalny wyprostowany dwupolowkowy dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
figure(21)
subplot(3,1,1)
plot(F(1,:),W(1,(1:length(YYY(1,:))/2)))
title('Sygnal sinusoidalny wyprostowany dwupolowkowy dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,2)
plot(F(2,:),W(2,(1:length(YYY(2,:))/2)))
title('Sygnal sinusoidalny wyprostowany dwupolowkowy dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,3)
plot(F(3,:),W(3,(1:length(YYY(3,:))/2)))
title('Sygnal sinusoidalny wyprostowany dwupolowkowy dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
%9 sinusoidalny wyprotowany jednopolowkowy
C13=A/(pi);
C14=A/2;
C15=-2*A/pi;
for j=1:3
zzz=0;
for i=1:1:n(:,j)
zzz=zzz+(1/(4*i*i-1))*cos(2*i*w*t);
end
Z(j,:)=(zzz*C15)+(sin(w*t)*C14)+C13;
N=length(zzz);
NN(j,:)=N;
fft_moc=fft(zzz(1:N));
moc_wid=fft_moc.*conj(fft_moc)/N;
widmo=sqrt(fft_moc.*conj(fft_moc))/N;
f=fs*(0:N/2-1)/N;
F(j,:)=f;
M(j,:)=moc_wid;
W(j,:)=widmo;
end
figure(22)
subplot(3,1,1)
plot(t,Z(1,:))
title('Sygnal sinusoidalny wyprostowany jednopolowkowy dla N=7');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,2)
plot(t,Z(2,:))
title('Sygnal sinusoidalny wyprostowany jednopolowkowy dla N=30');
xlabel('czas [s]');
ylabel('A');
subplot(3,1,3)
plot(t,Z(3,:))
title('Sygnal sinusoidalny wyprostowany jednopolowkowy dla N=99');
xlabel('czas [s]');
ylabel('A');
figure(23)
subplot(3,1,1)
plot(F(1,:),M(1,(1:length(Z(1,:))/2)))
title('Sygnal sinusoidalny wyprostowany jednopolowkowy dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,2)
plot(F(2,:),M(2,(1:length(Z(2,:))/2)))
title('Sygnal sinusoidalny wyprostowany jednopolowkowy dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
subplot(3,1,3)
plot(F(3,:),M(3,(1:(length(Z(3,:))/2))))
title('Sygnal sinusoidalny wyprostowany jednopolowkowy dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('g.w.');
figure(24)
subplot(3,1,1)
plot(F(1,:),W(1,(1:length(Z(1,:))/2)))
title('Sygnal sinusoidalny wyprostowany jednopolowkowy dla N=7');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,2)
plot(F(2,:),W(2,(1:length(Z(2,:))/2)))
title('Sygnal sinusoidalny wyprostowany jednopolowkowy dla N=30');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
subplot(3,1,3)
plot(F(3,:),W(3,(1:length(Z(3,:))/2)))
title('Sygnal sinusoidalny wyprostowany jednopolowkowy dla N=99');
xlabel('czestotliwosc [Hz]');
ylabel('|FFT(y)|');
