clc;clear all;close all;
% 1. Wygeneruj N=1000 probek sygnalu sinusoidalnego x(t)=Asin(2fxt) o amplitudzie A=5 i o czestotliwosci
% fx=10 Hz, sprobkowanego z czestotliwoscia fp=1000 Hz. Narysuj ten sygnal.
N=1000; 
A=5; 
fx=10; 
fp=1000; % Parametry sygnalu
dt=1/fp; % Okres probkowania
t=dt*(0:N-1); % Wektor chwil probkowania
 
x=A*sin(2*pi*fx*t); % Sygnal
figure(1) 
subplot(2, 2, 1)
plot(t,x); grid; title('Sygnal x(t)'); xlabel('czas [s]');  
% Oblicz wartosci parametrow sygnalu:
x_sred1=mean(x), x_sred2=sum(x)/N
 % wartosc srednia
x_max=max(x)
 % wartosc maksymalna
x_min=min(x)
 % wartosc minimalna
x_std1=std(x), x_std2=sqrt(sum((x-mean(x)).^2) / (N-1)) % odchylenie standardowe
x_eng=dt*sum(x.^2)
 % energia
x_moc=(1/N)*sum(x.^2)
 % moc srednia
x_skut=sqrt(x_moc)
 % wartosc skuteczna
% Oblicz i narysuj funkcje autokorelacji R1, R2 i R3 sygnalu x(n).
subplot(2, 2, 2)
R1=xcorr(x);
 % nieunormowana
R2=xcorr(x,'biased');
 % unormowana przez dlugosc /N
R3=xcorr(x);
 % unormowana przez /(N-abs(k))
tR=[-fliplr(t) t(2:N)]; plot(tR,R1); grid; title('Autokorelacja'); 

% Teraz sam wylicz R3============Nie dziala...
%for k=0:N-1
%R(k+1)= sum( x(1:N-k).*x(1+k:N) )/(N-k);
%end
%R = [ fliplr(R) R(2:N-1) ];
%subplot(2, 3, 2)
%plot(tR,R(1:length(tR))); grid; title('Autokorelacja wyliczona');

for tau = 0:N-1
    suma=0;
    for it = 1:N-tau
        suma=suma+(x(it)*x(it+tau));
    end
mojaRxx(tau+1) = suma/(N-tau)    ;
end
subplot(2,2,3)
plot([0:N-1], mojaRxx);grid


figure(2)
% Oblicz wspolczynniki zespolonego szeregu Fouriera dla tego sygnalu.
% Narysuj czesc rzeczywista, urojona i modul tych wspolczynnikow.
X = fft(x);
 % szybka dyskretna transformacja Fouriera
df = 1/(N*dt);
 % czestotliwosc podstawowa f0 = df = 1/T = 1/(N*dt)
f = df * (0 : N-1);
 % kolejne czestotliwosci w szeregu Fouriera
subplot(3, 2,1); 
plot(f,real(X)); grid; title('Real(X)'); xlabel('Hz');  
subplot(3,2,2); 
plot(f,imag(X)); grid; title('Imag(X)'); xlabel('Hz');  
subplot(3,2,3);
plot(f,abs(X)); grid; title('Abs(X)'); xlabel('Hz');  

subplot(3, 2, 4)
plot(f(1:N/2+1),abs(X(1:N/2+1))/(N/2)); grid; title('Po wyskalowaniu');  
% Zsyntezuj sygnal na podstawie wspolczynnikow jego szeregu Fouriera i porownaj z oryginalem.
xs=ifft(X);
subplot(3,2,[5, 6])
plot(t,real(x-xs)); grid; title({'Roznica sygnal,',' a sygnal odtworzony z fft'});  
% okolo 10^(14)


% 2. Wygeneruj N=1000 probek szumu o rozkladzie rownomiernym i normalnym (gaussowskim). Wyznacz dla nich
% funkcje autokorelacji, autokowariancji, histogram, szereg Fouriera i periodogram.
figure(3)
s1 = rand(1,N); s2=randn(1,N);
s = s2; % wybierz szum: s=s1 lub s=s2
subplot(3, 2, 1)
R = xcorr(s,'unbiased');
plot(tR,R); grid; title('Autokorelacja szumu');  
C = xcov(s);
subplot(3, 2, 2)
plot(tR,C); grid; title('Autokowariancja szumu');  
Hs = hist(s,100); % podziel os [xmin, xmax] na 100 podprzedzialow
subplot(3, 2, 3)
plot(Hs); title('Histogram szumu');  
S=fft(s);
subplot(3, 2, 4)
plot(f,abs(S)); grid; title('Widmo Fouriera szumu'); xlabel('f [Hz]');  
[Pss, fss]=periodogram(s,rectwin(length(s)),N/10,fp); % usrednij widma odcinkow sygnalu o dlugosci N/10 
subplot(3, 2, 5)
plot(fss,Pss); grid; title('Widmo usrednione'); xlabel('f [Hz]')
figure(4)
% 3. Dodaj sygnaly z punktu 1 i 2. Wyznacz i narysuj funkcje autokorelacji, autokowariancji i histogram sygnalu
% sumarycznego.
% 4. Powtorz operacje z punktu 3 po dodaniu do sygnalu 1+2 jeszcze jednej sinusoidy o czestotliwosci 250 Hz.
% 5. Zmoduluj w amplitudzie sygnal sinusoidalny z punktu pierwszego.
ampl = hamming(N)';
 % przykladowa funkcja modulujaca amplitude
y1 =ampl.*x;
 % modulacja amplitudy
subplot(3, 2, 1)
 plot(t,y1); grid; title('Sygnal z modulacja amplitudy');  
ampl = exp(-10*t);
 % eksponencjalna funkcja obwiedni amplitudy
y2 =ampl.*x;
 % modulacja
 subplot(3, 2, 2)
plot(t,y2); grid; title('Sygnal z modulacja amplitudy');  
% 6. Wygeneruj sygnal sinusoidalny z liniowa modulacja czestotliwosci (=x+t) oraz z sinusoidalna modulacja
% czestotliwosci ((=x +sin(mt)).
fx = 0; alfa = 10
 % wybierz, zaczynajac od malych wartosci; potem obserwuj
y3 = sin(2*pi*(fx*t + 0.5*alfa*t.^2));
subplot(3, 2, 3)
plot(t,y3); grid; title({'Sygnal z liniowa',' modulacja czestotliwosci'});  
fx = 10; fm = 2; df = 10;
 % czestotliwosci  nosna, modulujaca, glebokosc modulacji
y4 = sin(2*pi*(fx*t + df * sin(2*pi*fm*t)/(2*pi*fm)));
subplot(3, 2, 4)
plot(t,y4); grid; title({'Sygnal z sinusoidalna',' modulacja czestotliwosci'});  
% 7. Sklej dwa sygnaly.
y5 = [ y1 y4];
subplot(3, 2, 5)
plot(y5); grid; title('Sygnal sklejony');  
% 8. Splec ze soba dwa sygnaly, czyli dokonaj filtracji jednego z nich przez drugi
T = 5; N = 1000;
 % czas trwania sygnalow, liczba probek
dt = T/N; t = dt*(0:N);
 % okres probkowania, kolejne chwile czasowe
x = sin(2*pi*2*t)+0.5*sin(2*pi*8*t);
 % sygnal filtrowany: sinusoidy 2 Hz + 8 Hz
h = sin( 2*pi*2*t).*exp(-4*t);
 % sygnal filtrujacy
y = conv(x,h);
 % operacja filtracji (splot, konwolucja)
 figure(6)
subplot(311); plot(t,x); title('Sygnal WE x(t)');
subplot(312); plot(t,h); title('Odp impulsowa h(t)');
subplot(313); plot(t,y(1:N+1)); title('Sygnal WY h(t)');
 figure(7)
% 9. Skwantuj sygnal x(t) z punktu 8
x_min=-1.5; x_max=1.5; x_zakres=x_max-x_min;
Nb=3; Nq=2^Nb;
dx=x_zakres/Nq;
xq=dx*round(x/dx);
clf; plot(t,xq);title('skwantowany sygnal')
% minimum, maksimum, zakres
% liczba bitow, liczba przedzialow kwantowania
% szerokosc przedzialu kwantowania
% kwantyzacja sygnalu
% pokaz sygnal po skwantowaniu

