\contentsline {chapter}{\numberline {1}Laboratorium 1}{4}%
\contentsline {section}{\numberline {1.1}Cel laboratorium}{4}%
\contentsline {section}{\numberline {1.2}Programy}{4}%
\contentsline {chapter}{\numberline {2}Laboratorium 2}{6}%
\contentsline {section}{\numberline {2.1}Cel laboratorium}{6}%
\contentsline {section}{\numberline {2.2}Programy}{6}%
\contentsline {chapter}{\numberline {3}Laboratorium 3}{9}%
\contentsline {section}{\numberline {3.1}Cel laboratorium}{9}%
\contentsline {section}{\numberline {3.2}Programy}{9}%
\contentsline {chapter}{\numberline {4}Laboratorium 4}{11}%
\contentsline {section}{\numberline {4.1}Cel laboratorium}{11}%
\contentsline {section}{\numberline {4.2}Programy}{11}%
\contentsline {chapter}{\numberline {5}Laboratorium 5}{17}%
\contentsline {section}{\numberline {5.1}Cel laboratorium}{17}%
\contentsline {section}{\numberline {5.2}Programy}{17}%
\contentsline {chapter}{\numberline {6}Laboratorium 6}{33}%
\contentsline {section}{\numberline {6.1}Cel laboratorium}{33}%
\contentsline {section}{\numberline {6.2}Programy}{33}%
\contentsline {chapter}{\numberline {7}Laboratorium 7}{47}%
\contentsline {section}{\numberline {7.1}Cel laboratorium}{47}%
\contentsline {section}{\numberline {7.2}Programy}{47}%
\contentsline {chapter}{\numberline {8}Zieliński - Sygnały i ich parametry }{59}%
\contentsline {section}{\numberline {8.1}Opis działania programów}{59}%
\contentsline {section}{\numberline {8.2}Programy}{59}%
\contentsline {chapter}{\numberline {9}Zieliński - Podstawy matematyczne analizy sygnałów deterministycznnych }{68}%
\contentsline {section}{\numberline {9.1}Opis działania programów}{68}%
\contentsline {section}{\numberline {9.2}Programy}{68}%
\contentsline {chapter}{\numberline {10}Zieliński - Szereg Fouriera}{72}%
\contentsline {section}{\numberline {10.1}Opis działania programów}{72}%
\contentsline {section}{\numberline {10.2}Programy}{72}%
\contentsline {chapter}{\numberline {11}Zieliński - Całkowe przekształcenie Fouriera}{80}%
\contentsline {section}{\numberline {11.1}Opis działania programów}{80}%
\contentsline {section}{\numberline {11.2}Programy}{80}%
\contentsline {chapter}{\numberline {12}Zieliński - Układy analogowe}{84}%
\contentsline {section}{\numberline {12.1}Opis działania programów}{84}%
\contentsline {section}{\numberline {12.2}Programy}{84}%
\contentsline {chapter}{\numberline {13}Zieliński - Analogowe filtry Czebyszewa i Butterwortha}{90}%
\contentsline {section}{\numberline {13.1}Opis działania programów}{90}%
\contentsline {section}{\numberline {13.2}Programy}{90}%
\contentsline {chapter}{\numberline {14}Zieliński - Analiza częstotliwościowa sygnałów dyskretnych }{93}%
\contentsline {section}{\numberline {14.1}Opis działania programów}{93}%
\contentsline {section}{\numberline {14.2}Programy}{93}%
\contentsline {chapter}{\numberline {15}Zieliński - Algorytmy wyznaczania dyskretnej transformacji Fouriera}{99}%
\contentsline {section}{\numberline {15.1}Opis działania programów}{99}%
\contentsline {section}{\numberline {15.2}Programy}{99}%
\contentsline {chapter}{\numberline {16}Zieliński - Układy dyskretne}{101}%
\contentsline {section}{\numberline {16.1}Opis działania programów}{101}%
\contentsline {section}{\numberline {16.2}Programy}{101}%
\contentsline {chapter}{\numberline {17}Zieliński - Projektowanie rekursywnych filtrów cyfrowych}{105}%
\contentsline {section}{\numberline {17.1}Opis działania programów}{105}%
\contentsline {section}{\numberline {17.2}Programy}{105}%
\contentsline {chapter}{\numberline {18}Zieliński - Projektowanie nierekursywnych filtrów cyfrowych}{110}%
\contentsline {section}{\numberline {18.1}Opis działania programów}{110}%
\contentsline {section}{\numberline {18.2}Programy}{110}%
\contentsline {chapter}{\numberline {19}Zieliński - Metody czasowo- częstotliwościowej analizy sygnałów}{125}%
\contentsline {section}{\numberline {19.1}Opis działania programów}{125}%
\contentsline {section}{\numberline {19.2}Programy}{125}%
