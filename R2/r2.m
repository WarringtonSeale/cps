% Transformaty ortogonalne sygnalow
% 1) ksztalt dyskretnych baz: Fouriera, kosinusowej, sinusowej, Hadamarda, Walsha
% 2) dopasowanie bazy  przykladowa dekompozycja dwoch sygnalow
clear all; subplot(111);
N=4;
n=0:N-1;
NN=2*N;
% wybor liczby (dlugosci) funkcji bazowych (wymiar przestrzeni wektorowej)
% indeksy wszystkich probek poszczegolnych funkcji bazowych
% zmienna pomocnicza
% Ksztalt funkcji bazowych dla transformacji kosinusowej i sinusowej
% n-ta probka k-tej funkcji bazowej
f = 1/sqrt(N); % wspolczynnik normalizujacy  transformacje Fouriera
c = [ sqrt(1/N) sqrt(2/N)*ones(1,N-1) ]; % wspolczynnik normalizujacy transformacje kosinusowa
s = sqrt(2/(N+1)); % wspolczynnik normalizujacy transformacje sinusowa
for k=0:N-1
 % wyznacz wszystkie probki k-tej bazy
bf(k+1,n+1) = f * exp(j*2*pi/N*k*n);
 % transformacja Fouriera
bc(k+1,n+1) = c(k+1) * cos( pi*k*(2*n+1) / NN );
 % transformacja kosinusowa
bs(k+1,n+1) = s* sin( pi*(k+1)*(n+1) / (N+1) ); % transformacja sinusowa
% stem(bc(k+1,1:N)); title('k-ta funkcja bazowa');  
end
% Ksztalt funkcji bazowych dla transformacji Hadamarda
% n-ta probka k-tej funkcji bazowej

m=log2(N); c=sqrt(1/N);
for k=0:N-1
kk=k;
for i=0:m-1
ki(i+1)=rem(kk,2); kk=floor(kk/2);
end
for n=0:N-1
nn=n;
for i=0:m-1
ni(i+1)=rem(nn,2); nn=floor(nn/2);
end
bHD(k+1,n+1) = c * (-1)^sum(ki .* ni);
end
% stem(bHD(k+1,1:N)); title('k-ta funkcja bazowa');  
end
% Ksztalt funkcji bazowych dla transformacji Haara
% n-ta probka k-tej funkcji bazowej
c=sqrt(1/N); bHR(1,1:N)=c*ones(1,N);
for k=1:N-1
p=0;
while(k+1 > 2^p)
p=p+1;
end
p=p-1;
q=k-2^p+1;
for n=0:N-1
x=n/N;
if ( ( (q-1)/2^p <= x ) & ( x < (q-1/2)/2^p ) ) bHR(k+1,n+1)= c*2^(p/2);
elseif ( ( (q-1/2)/2^p <= x ) & ( x < q/2^p )) bHR(k+1,n+1)=-c*2^(p/2);
else bHR(k+1,n+1)=0;
end
end
% stem(bHR(k+1,1:N)); title('k-ta funkcja bazowa');  
end
% Sprawdzenie ortonormalnosci wybranych funkcji bazowych
for k=1:N
Tf(k,1:N)= bf(k,1:N);
Tc(k,1:N) = bc(k,1:N);
Ts(k,1:N) = bs(k,1:N);
THD(k,1:N) = bHD(k,1:N);
THR(k,1:N) = bHR(k,1:N);
end
T = THR;
I = T * T'
 
% zbudowanie macierzy transformacji
% transformacja Fouriera
% transformacja kosinusowa
% transformacja sinusowa
% transformacja Hadamarda
% transformacja Haara
% wybierz transformacje
% sprawdz, czy iloczyn jest macierza diagonalna jednostkowa
% Przyklad analizy (dekompozycji) i syntezy sygnalu
% Generacja sygnalow testowych
kk = 2;
fi = 0;
% testowy indeks czestotliwosci, np. 1.35, 2, 2.5, 3
% przesuniecie fazowe 0, pi/8, pi/4, pi/2
n = 0 : N-1;
x1 = cos( (2*pi/N)*kk*n + fi );
 % cz. rzeczywista bazy fourierowskiej
x2 = cos( pi*kk*(2*n+1)/NN + fi );
 % wektor bazy kosinusowej
x3 = sin( pi*(kk+1)*(n+1)/(N+1) + fi );
 % wektor bazy sinusowej
x4 = cos( (2*pi/N)*2*n + fi ) + cos( (2*pi/N)*4*n + fi );
x5 = [ ones(1,N/2) zeros(1,N/2) ];
x6 = [ -ones(1,N/4) ones(1,N/2) -ones(1,N/4) ];
x = x6;
T = THR;
% wybor konkretnego sygnalu do dekompozycji
% wybor transformacji: Tf, Tc, Ts, THD, THR

a = T * x';
y = T' * a;
y = y';
% analiza w zapisie macierzowym
% synteza w zapisie macierzowym
% zamien wektor pionowy na poziomy
figure(1)
subplot(3, 2, 1)
stem(n,x,'filled','-k'); axis tight; title('sygnal analizowany x(l)');
xlabel('numer probki');  
subplot(3, 2, 2)
stem(n,real(a),'filled','-k'); axis tight; title('wsp dekomopozycji alfa(k)');
xlabel('numer probki');  
subplot(3, 2, 3)
stem(n,y,'filled','-k'); axis tight; title('sygnal zsyntezowany x(l)');
xlabel('numer probki');  
subplot(3, 2, 4)
stem(n,y-x,'filled','-k'); axis tight; title('blad syntezy 1: y(l)-x(l)');
xlabel('numer probki');  
% Analiza i synteza w zapisie niemacierzowym
y=zeros(1,N);
 %
for k = 0 : N-1
 % ANALIZA: oblicz wspolczynniki
a(k+1) = sum( x .* conj(T(k+1,1:N)) );
 %
end
 %
for k = 0 : N-1
 % SYNTEZA: odtworz sygnal
y = y + a(k+1) * T(k+1,1:N);
 %
end
 %
 subplot(3, 2, [5, 6])
stem(n,y-x,'filled','-k'); axis tight; title('blad syntezy 2: y(l)-x(l)');
xlabel('numer probki');  


