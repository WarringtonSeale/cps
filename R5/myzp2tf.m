%wlasna implementacja funkcji zp2tf przeksztalcajaca bieguny i zera na
%transmitancje

clc;clear;close;

z1 = 5;
z2 = 15;
z = i*[-z2, -z1, z1, z2];
p1 = 9.5;
p2 = 10.5;
dRe = 0.5;
p = [-dRe-i*p2, -dRe-i*p1, -dRe+i*p1, -dRe+i*p2]; %przykladowe dane 


nL = length(z); %- stopien licznika(ilosc zer)
nP = length(p); %-stopien mianownika(ilosc biegunow)

syms s
L=1
M=1
for it = [1:1:nL]
L=L*(s-z(it)) %Tworzymy symboliczne wyrazenie ze skldnikow (s-zero)
end
myb = sym2poly(L);

for it = [1:1:nP]
    M=M*(s-p(it));
end
mya = sym2poly(M);
%Sprawdzamy obliczenia porownujac wynik z wbudowana funkcja matlaba
[b, a] = zp2tf(z', p', 1);

if mya == a 
    fprintf('Wartosci w mianowniku sa zgodne\n')
end
if myb == b
    fprintf('Wartosci w liczniku sa zgodne\n') 
end
