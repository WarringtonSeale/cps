clc;clear;close all;

%_________________________________Bandpass - projektowanie filtra

%definiujemy odleglosc zer od 0:
z1 = 5;
z2 = 15;
%i przeksztalcamy na zespolone:
z = i*[-z2, -z1, z1, z2];
%odleglosc biegunow od osi urojonej :
p1 = 9.5;
p2 = 10.5;
% i od osi rzeczywistej:
dRe = 0.5
%i myk na zespolone:
p = [-dRe-i*p2, -dRe-i*p1, -dRe+i*p1, -dRe+i*p2]
%----------------wykresy:
%zer i biegunow
figure('Name', 'Transmitancja z zer i biegunow')
subplot(4, 2, 1)
plot(real(z), imag(z), 'or'); %wrzucamy na wykres zera
hold on;
plot(real(p), imag(p), 'xb') %i bieguny
hold off;
grid;xlabel('Re');ylabel('Im');xlim([-1, 1]);ylim([-12, 12]);title('Wykres zer i biegunow')
%charakterystyki amplitudowe
w = [0:0.01:10];
[b, a] = zp2tf(z', p', 1); %Wyznaczamy wspolczynniki w transmitancji
H = freqs(b, a, w); %wyznaczamy widmo transmitancji
Hm = abs(H);    %wyznaczamy modul transmitancji
Hmb = 20*log10(Hm); %wyznaczamy modul transmitancji w db

subplot(4, 2, 3)
plot(w, Hm);title('Charakterystyka amplitudowa');xlabel('\omega[rad/s]');ylabel('wzmocnienie[-]');grid;
subplot(4, 2, 4)
plot(w, Hmb);title('Charakterystyka amplitudowa');xlabel('\omega[rad/s]');ylabel('wzmocnienie[dB]');grid;
% Charakterystyki fazowe

Hf = angle(H); Hfu = unwrap(Hf); %unwrap powoduje ze mozemy rozroznic x od x+2*pi 
subplot(4, 2, 5)
plot(w, Hf);title('Charakterystyka fazowa');xlabel('\omega[rad/s]');ylabel('\phi[rad]');grid;
subplot(4, 2, 6)
plot(w, unwrap(Hf));title('Charakterystyka fazowa');xlabel('\omega[rad/s]');ylabel('\phi[rad]');grid;
% Odpowiedz impulsowa

Th = 15 %horyzont czasowy
h = impulse(b, a, Th); %wyznaczanie odpowiedzi funkcja z Controll Toolbox'a
dt = Th/(length(h)-1); %wyznaczenie kroku czasowego
t = [0:dt:Th]; %stworzenie wektora czasu do wykresu
subplot(4, 2, 7)
plot(t, h);title('Odpowiedz impulsowa');xlabel('t[s]');ylabel('amplituda [-]');grid

%Odpowiedz skokowa

u = step(b, a, Th);
subplot(4, 2, 8)
plot(t, u);title('Odpowiedz skokowa');xlabel('t[s]');ylabel('amplituda [-]');grid

%_________________________________ Szukanie zer i biegunow z
%transmitancji(bandstop)

a = [4, 5, 3, 1];%wspolczynniki w mianowniku
b = [0.3, 0, 1];%wspolczynniki w liczniku

[z, p, amp] = tf2zp(b, a); %szukanie zer i biegunow
z = z' %transformacja na wektor poziomy
p = p'

%----------------wykresy: - to samo co w poprzednim
%zer i biegunow
figure('Name', 'Zera i bieguny z transmitancji')
subplot(4, 2, 1)
plot(real(z), imag(z), 'or'); %wrzucamy na wykres zera
hold on;
plot(real(p), imag(p), 'xb') %i bieguny
hold off;
grid;xlabel('Re');ylabel('Im');xlim([-2, 2]);ylim([-5, 5]);title('Wykres zer i biegunow')
%charakterystyki amplitudowe
w = [0:0.01:10];
[b, a] = zp2tf(z', p', 1); %Wyznaczamy wspolczynniki w transmitancji
H = freqs(b, a, w); %wyznaczamy widmo transmitancji
Hm = abs(H);    %wyznaczamy modul transmitancji
Hmb = 20*log10(Hm); %wyznaczamy modul transmitancji w db

subplot(4, 2, 3)
plot(w, Hm);title('Charakterystyka amplitudowa');xlabel('\omega[rad/s]');ylabel('wzmocnienie[-]');grid;
subplot(4, 2, 4)
plot(w, Hmb);title('Charakterystyka amplitudowa');xlabel('\omega[rad/s]');ylabel('wzmocnienie[dB]');grid;
% Charakterystyki fazowe

Hf = angle(H); Hfu = unwrap(Hf) %unwrap powoduje ze mozemy rozroznic x od x+2*pi 
subplot(4, 2, 5)
plot(w, Hf);title('Charakterystyka fazowa');xlabel('\omega[rad/s]');ylabel('\phi[rad]');grid;
subplot(4, 2, 6)
plot(w, unwrap(Hf));title('Charakterystyka fazowa');xlabel('\omega[rad/s]');ylabel('\phi[rad]');grid;
% Odpowiedz impulsowa

Th = 15 %horyzont czasowy
h = impulse(b, a, Th); %wyznaczanie odpowiedzi funkcja z Controll Toolbox'a
dt = Th/(length(h)-1); %wyznaczenie kroku czasowego
t = [0:dt:Th]; %stworzenie wektora czasu do wykresu
subplot(4, 2, 7)
plot(t, h);title('Odpowiedz impulsowa');xlabel('t[s]');ylabel('amplituda [-]');grid

%Odpowiedz skokowa

u = step(b, a, Th);
subplot(4, 2, 8)
plot(t, u);title('Odpowiedz skokowa');xlabel('t[s]');ylabel('amplituda [-]');grid


%_________________________________ Projekt filtra wyzszego rzedu z biegunow
%i zer (highpass)

ReZ = [0, 0, 0, 0, 0, 0, 0]; % Wartosci rzeczywiste kolejnych zer
ImZ = [-3, -2, -1, 0, 1, 2, 3]; %wartosci urojone kolejnych zer
z = ReZ + i*ImZ %zera

ReP = [-1, -1, -1, -1, -1, -1, -1];
ImP = [-3, -2, -1, 0, 1, 2, 3];
p = ReP + i*ImP

%----------------wykresy: - to samo co w poprzednim
%zer i biegunow
figure('Name', 'Filtr wyzszego rzedu z zer i biegunow')
subplot(4, 2, 1)
plot(real(z), imag(z), 'or'); %wrzucamy na wykres zera
hold on;
plot(real(p), imag(p), 'xb') %i bieguny
hold off;
grid;xlabel('Re');ylabel('Im');xlim([-2, 2]);ylim([-4, 4]);title('Wykres zer i biegunow')
%charakterystyki amplitudowe
w = [0:0.01:10];
[b, a] = zp2tf(z', p', 1); %Wyznaczamy wspolczynniki w transmitancji
H = freqs(b, a, w); %wyznaczamy widmo transmitancji
Hm = abs(H);    %wyznaczamy modul transmitancji
Hmb = 20*log10(Hm) %wyznaczamy modul transmitancji w db

subplot(4, 2, 3)
plot(w, Hm);title('Charakterystyka amplitudowa');xlabel('\omega[rad/s]');ylabel('wzmocnienie[-]');grid;
subplot(4, 2, 4)
plot(w, Hmb);title('Charakterystyka amplitudowa');xlabel('\omega[rad/s]');ylabel('wzmocnienie[dB]');grid;
% Charakterystyki fazowe

Hf = angle(H); Hfu = unwrap(Hf); %unwrap powoduje ze mozemy rozroznic x od x+2*pi 
subplot(4, 2, 5)
plot(w, Hf);title('Charakterystyka fazowa');xlabel('\omega[rad/s]');ylabel('\phi[rad]');grid;
subplot(4, 2, 6)
plot(w, unwrap(Hf));title('Charakterystyka fazowa');xlabel('\omega[rad/s]');ylabel('\phi[rad]');grid;
% Odpowiedz impulsowa

Th = 15 %horyzont czasowy
h = impulse(b, a, Th); %wyznaczanie odpowiedzi funkcja z Controll Toolbox'a
dt = Th/(length(h)-1); %wyznaczenie kroku czasowego
t = [0:dt:Th]; %stworzenie wektora czasu do wykresu
subplot(4, 2, 7)
plot(t, h);title('Odpowiedz impulsowa');xlabel('t[s]');ylabel('amplituda [-]');grid

%Odpowiedz skokowa

u = step(b, a, Th);
subplot(4, 2, 8)
plot(t, u);title('Odpowiedz skokowa');xlabel('t[s]');ylabel('amplituda [-]');grid

