clc; close all; clear;

nf1 = 4;
mf1 = 2;

N = 256;
f_s = 1000;    %Hz - czestotliwosc probkowania
dt = 1 / f_s;
t = [0:dt:(N-1)*dt];
f_x = 100;   %Hz - czestotliwosc sygnalu
w_x = 2*pi*f_x;
tR = -(N-1)*dt:dt:(N-1)*dt;


x = sin(w_x) + randn(1, N); %Generowanie sygnalu analizowanego
xc = x;
figure(1)
subplot(nf1, mf1, 1)
plot(t, x);title('Analizowany sygnal');grid;xlabel('t[s]');ylabel('wartosc[-]')

% Obliczanie autokorelacji wykorzystujac procedury MATLAB'a

R1 = xcorr(x, x, 'biased');
subplot(nf1, mf1, 2);
plot(tR, R1);title('Autokorelacja MATLAB');xlabel('\tau[s]');ylabel('wartosc[-]');grid;

% Obliczanie autokorelacji wykorzystujac definicje

R2 = [];
for k = [0:(N-1)]
        R2(k+1) = sum(x(k+1:N).*x(1:N-k)) / N;
end
R2 = [R2(N:-1:1), R2(2:1:N)];


subplot(nf1, mf1, 3);
plot(tR, R2);title('Autokorelacja recznie');xlabel('\tau[s]');ylabel('wartosc[-]');grid;

% Blad pomiedzy procedura MATLAB'a a obliczeniami z definicji
dR = R1 - R2;
subplot(nf1, mf1, 4);
plot(tR, dR);title('Roznica miedzy dwoma algorytmami');xlabel('\tau[s]');ylabel('wartosc[-]');grid;

% z FFT

M = N/8;
x = [x, zeros(1, M-1)];
X = fft(x);
X = X.*conj(X);
R3 = ifft(X);
R3 = real(R3(1:M))/N;
subplot(nf1, mf1, 5)
k = [1:M];
plot(t(k),R3(k));title('Autokorelacja z FFT');xlabel('\tau[s]');ylabel('wartosc[-]');grid;
% estymacja funkcji gestosci widmowej mocy (FFT)
%met. Blackmana-tUkleya

L = N;
w = hanning(2*M-1);
w=w(M:2*M-1);
w = w';
Rw = R3 .* w;
s = [ Rw(1:M), zeros(1, L-2*M+1), Rw(M:-1:2)];
S = real(fft(s));
df = 1/(L*dt);
f = [0:df:(L-1)*df];
k = 1:L/2+1;
subplot(nf1, mf1, 6);
plot(f(k), S(k));title('fft z definicji');xlabel('f[Hz]');ylabel('Wartosc[-]')

% porownanie FFT z periodogramem

w = hanning(N)';
S1 = abs( fft( xc.*w )  ).^2;

S1 = S1 / (sum(w.*w));
df = f_s/N; f = 0:df:(N-1)*df; k = 1:L/2-1;

subplot(nf1, mf1, 7);
plot(f(k), S1(k));grid;title('Periodogram calego sygnalu');xlabel('f [Hz]');ylabel('wartosc[-]');

%porownanie FFT z periodogramem usrednionym
Nfft = N;
Nwind = M;
Noverlap = M/2;

Nshift = Nwind-Noverlap;

K = floor((N-Nwind)/Nshift)+1;
w = hanning(Nwind)';
S2 = zeros(1, Nfft);
for k = [1:K]
    n = [1+(k-1)*Nshift : Nwind+(k-1)*Nshift];
        bx = x(n);
        bx = bx.*w;
        bxz = [bx, zeros(1,Nfft-Nwind)];
        X = fft(bxz, Nfft);0
        S2 = S2+abs(X).^2;
end
S2 = S2/(K*sum(w.*w));
df = f_s/Nfft; f2 = 0:df:(Nfft-1)*df; k = 1:Nfft/2 +1;
%[S2, f2] = psd(xc, Nfft, f_s, w, Noverlap);
[S2, f2] = periodogram(xc, hanning(length(xc)));
subplot(nf1, mf1, 8);
plot(f2(k), S2(k)); grid;
title('Usredniony periodogram fragmentow sygnalu'); xlabel('f [Hz]'); ylabel('wartosc[-]')
