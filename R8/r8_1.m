clc;
clear all;
clf;
nf1 = 4;
mf1 = 2;
% wartosci parametrow analizy
f_s = 16;
df = 1;
Asl = 80;
dfn = df/f_s;
Dml = 2*pi*dfn; %rad/s

e = exp(1);

% okno Kaisera - parametry
if Asl <= 13.26 
    beta = 0;
end

if Asl>13.26 && Asl<60
    beta = 0.76609*(Asl-13.26)^0.4 + 0.09834 * Asl-13.26;
end

if Asl>60 && Asl<=120
    beta = 0.12438*(Asl+6.3);
end

N = ceil(24*pi*(Asl+12)/(155*Dml)) +1;
beta;

%Generowanie sygnalu

N = 32;
ax1 = 1;
ax2 = 0.001;
fx1 = 1;
fx2 = 2;
fm = 8;
NF = 32;

dt = 1/f_s;
t = [0:(N-1)]*dt;
x1 = ax1 * cos(2*pi*fx1*t);
x2 = ax2 * cos(2*pi*fx2*t);    
x12 = x1+x1;
x = x12;

figure(1)
subplot(nf1, mf1, 1)
stem(t, x, '-ko', 'filled'); axis tight; grid;title('sygnal');xlabel('t[s]');ylabel('wartosc [-]');

%generowanie okna

w1 = boxcar(N);
w2 = bartlett(N);
w3 = hanning(N);
w4 = blackman(N);
w6 = kaiser(N, beta);

w = w3;

skala = sum(boxcar(N))/sum(w); w = skala*w;
subplot(nf1, mf1, 2)
plot(w);grid;axis tight;title('okno');xlabel('nr probki');


W = fft(w);

f = f_s*(0:(N-1))/N;

W = abs(W)/N; W = 20*log10(W);
subplot(nf1, mf1, 3)
plot(f, W);grid;axis tight;title('FFT z okna');xlabel('f');
xw = x.*w';

df = 2*fm/(NF-1); f = -fm:df:fm;
X = freqz(xw, 1, f, f_s); X = 2*abs(X)/N; X = 20*log10(X);
subplot(nf1, mf1, 4)
plot(f, X);grid;axis tight;title('widmo okresowe niesporbkowane');xlabel('f[Hz]');
subplot(nf1, mf1, 5)

[X1, f1] = freqz(xw, 1, NF, 'whole', f_s);
X1 = 2*abs(X1)/N;
X1 = 20*log10(X1);
plot(f1, X1);grid;axis tight;title('1 okres- niesporbkowane');xlabel('f[Hz]');
subplot(nf1, mf1, 6)
n = 0:N-1;
for k=0:N-1
    X2(k+1)=sum(xw.*exp(-j*2*pi*k*n/N));
end
f0 = 1/(N*dt); f2 = 0:f0:(N-1)*f0;
X2 = 2*abs(X2)/N;
X2 = 20*log10(X2);
plot(f1, X1, '-k', f2, X2, 'ko', 'MarkerFaceColor', 'k');grid;axis tight;title('porownanie widm z wlasnej funkcji i freqz()');xlabel('f[Hz]');
subplot(nf1, mf1, 7)
X2=fft(xw);
X2 = 2*abs(X2)/N; X2 = 20*log10(X2);
plot(f1, X1, '-k', f2, X2, 'ko', 'MarkerFaceColor', 'k');grid;axis tight;title('porownanie widm z fft() i freqz()');xlabel('f[Hz]');

%demonstracja interpolacji metoda dodawania zer 

Nx = N; x = x(1:Nx);
w = kaiser(Nx, beta);
skala = sum(boxcar(Nx))/sum(w); w = skala.*w';
xw = x.*w;
Niter = 8; Nfft = 128;
for iter = 1:Niter
    X = 20*log10(2*abs(fft(xw, Nfft))/Nx);
    df = f_s/Nfft; f = 0:df:(Nfft-1)*df; k=1:Nfft/2+1;
    plot(f(k), X(k), 'ko', f(k), X(k), 'b-');grid;title('interpolacja widma zerami i dftfft')
    Nfft = 2*Nfft;
end
  
