clc;clear;close;
A = 0.5;
B = 0.7;
f1 = 420;
f2 = 911;
fs = 3200;

w1 = 2*pi*f1;
w2 = 2*pi*f2;

t_k = 0.05;

t = [0:1/fs:t_k];

y1 = A*sin(w1*t);
y2 = B*sin(w2*t);
y3 = y1+y2;
y4 = y1-y2;

data = [y1; y2; y3; y4; t];

outputfile = fopen('data.bin', 'w');
fprintf(outputfile, 'y1\ty2\ty3\ty4\tt\n');
fprintf(outputfile, '%12.6f\t%12.6f\t%12.6f\t%12.6f\t%12.6f\n', data);
fclose(outputfile);

inputfile = fopen('data.bin', 'r');
fgetl(inputfile);
datafrombin = fscanf(inputfile, '%f\t%f\t%f\t%f\t%f\t', [5, Inf]);

for i = [1:4]
    figure(i);
    subplot(2, 1, 1);
    plot(data(5, :), data(i, :));
    xlabel('t[s]');
    ylabel(sprintf('y_%d(t)[-]', i));
    title('przebieg');
    
    subplot(2, 1, 2);
    plot(datafrombin(5, :), datafrombin(i, :))
    xlabel('t[s]');
    ylabel(sprintf('y_%d(t)[-]', i));
    title('przebieg odczytany z zapisanego pliku bin');   
end

%parametry; k1 - max; k2 - min; k3 - sredn; k4 - energ; l1-y1, l2-y2...
params = [];
for i = [1:4]
    %max:
    params(i, 1) = max(data(i, :));
    %min:
    params(i, 2) = min(data(i, :));
    %avg:
    params(i, 3) = (sum(data(i, :)))/(length(data(i,:)));
    %ene:
    params(i, 4) = sum(data(i, :).^2);
end
figure(6)
    ids = [1; 2; 3; 4];
    h = {'ID', 'max', 'min', 'srednia', 'energia'}
    f = figure
    t = uitable(f, 'data', [ids,params], 'columnname', h )
params