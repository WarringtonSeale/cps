clc
clear all
close all
prettyFigures(5.5, 7)
%sygnal
fs=1000;
f=100;
Bs=[0.0675,  0.1349,  0.0675;...
    0.0412,  0.0824,  0.0412;...
    0.0996,  0.1297,  0.0996;...
    0.1239,  0.0662,  0.1239];

As=[1,      -1.1430,  0.4128;...
    1,      -1.4409,  0.6737;...
    1,      -1.6099,  0.6794;...
    1,      -1.4412,  0.6979];

for i = 1:4
A = As(i,:);
B = Bs(i,:);

sys=tf(B,A);
%delta kroneckera
x=zeros(1,101);
x(1,1)=1;
%wykresy
[h,w]=freqz(B,A);
M=10*log(abs(h));
Fi=phase(h); %rad
Fii=(180/pi)*Fi;
w=w/pi;
figure('name', sprintf('M: %1.4f, %1.4f, %1.4f; L: %1.4f, %1.4f, %1.4f', B(1),B(2),B(3), A(1), A(2), A(3)))
subplot(4,2,1)
plot(w,M)
ylabel('Modul [dB]')
xlabel('\omega_n ')
grid on
subplot(4,2,3)
plot(w,Fii)
ylabel('Faza [deg]')
xlabel('\omega_n')
grid on
subplot(4, 2, [5, 7])
dimpulse(B,A);grid

y=filter(B,A,x);
t=0:(1/fs):0.1;
subplot(4,2,2)
plot(t,x);grid;
xlabel('czas [s]');
ylabel('sygnal');
subplot(4,2,4)
plot(t,y);grid;
xlabel('czas [s]');
ylabel('sygnal po filtracji');
subplot(4, 2, [6, 8])
a=0;
b=0;
r=1;
x = linspace(a-r,a+r,100);
y1=sqrt(r^2-(x-a) .^2)+b;
y2=-sqrt(r^2-(x-a) .^2)+b;
plot(x,[y1; y2],'b')
grid on
axis equal
hold on
pzmap(B,A)%funkcja znajdujaca zera, bieguny i rysuje je na wykresie
[z,p,k]=tf2zpk(B,A);
end
