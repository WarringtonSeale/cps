clc
clear all
close all
prettyFigures(5.5, 5.5)
f1=100;
f2=250;
f3=400;
fs=1000;
A1=1;
A2=0.8;
A3=0.65;
t=0:(1/fs):0.1;
x=A1*sin(2*pi*f1*t)+A2*sin(2*pi*f2*t)+A3*sin(2*pi*f3*t);
rodzajefiltrow = ["lowpass 300Hz", "lowpass 200Hz", "bandpass  200-300Hz", "highpass 300Hz"];
for i = [1:4]

%Projektowanie filtra i filtracja

    fo1=300;
    fo2=200;
    wn1=2*fo1/fs;
    wn2=2*fo2/fs;
    N=[2,4,8];

if i == 1    %filtr butter dolnoprzepustowy
    [B,A]=butter(N(1,3),wn1);
elseif i==2
    [B,A]=butter(N(1,3),wn2);
    
elseif i==3
    [B,A]=butter(N(1,3),[wn2 wn1]);
    
elseif i==4
    [B,A]=butter(N(1,3),wn1,'high');
end

       



%filtracja
xf=filter(B,A,x);
%widmo po i przed
n=length(x);
fft_moc=fft(x(1:n));
moc_wid=fft_moc.*conj(fft_moc)/n;
f=fs*(0:n/2-1)/n;
widmo=sqrt(fft_moc.*conj(fft_moc))/n;
%F=[sygnal,przefiltrowany] W=[sygn,filtrowany]
F(1,:)=f;
W(1,:)=widmo;
n=length(xf);
fft_moc=fft(xf(1:n));
moc_wid=fft_moc.*conj(fft_moc)/n;
f=fs*(0:n/2-1)/n;
widmo=sqrt(fft_moc.*conj(fft_moc))/n;
F(2,:)=f;
W(2,:)=widmo;
figure('name', sprintf("filtr %s", rodzajefiltrow(i)));
subplot(3, 2, 1)
plot(t,x);grid
xlabel('czas [s]');
ylabel('amplituda');
title('sygnal oryginalny');
subplot(3, 2, 3)
plot(t,xf);grid
xlabel('czas [s]');
ylabel('amplituda');
title('po filtracji');

subplot(3, 2, 2)
plot(F(1,:),W(1,(1:length(x)/2)));grid
xlabel('czestotliwosc [Hz]');
ylabel('modul FFT');
title('sygnal oryginalny');
subplot(3, 2, 4)
plot(F(2,:),W(2,(1:length(xf)/2)));grid
xlabel('czestotliwosc [Hz]');
ylabel('modul FFT');
title('po filtracji');


[h,w]=freqz(B,A);
hm=log10(abs(h));
Fi=phase(h); %rad
Fii=(180/pi)*Fi;
w=w/pi;
subplot(3, 2, 5)
plot(w,hm);grid
ylabel('Modul [dB]')
xlabel('\omega_n ')
grid on
subplot(3, 2, 6)
plot(w,Fii);grid
ylabel('Faza [deg]')
xlabel('\omega_n')
grid on
end


