clc
clear all
close all
format long e
F=[0 0.1 0.2 0.5 0.7 1];
M=[1 1 1 0 0 0];
% F=[0 0.1 0.7 1];
% M=[1 1 0 0];
N=[2,4,8];
[B,A]=yulewalk(8,F,M);
% [B1,A1]=yulewalk(N(1,2),F,M);
% [B2,A2]=yulewalk(N(1,3),F,M);
% % freqz(B,A,128);
% hold on
% [h1,w1]=freqz(B1,A1);
% MM=20*log(abs(h1));
% Fi=phase(h1); %rad
% Fii=(180/pi)*Fi;
% subplot(2,1,1)
% plot(w1,MM,'r')
% subplot(2,1,2)
% plot(w1,Fii,'r')
% hold on
% freqz(B,A);
b=fir2(128,F,M);
fir2(N(1,3),F,M);
figure(1)
[h,w]=freqz(b,1);
M=20*log(abs(h));
Fi=phase(h); %rad
Fii=(180/pi)*Fi;
w=w/pi;
subplot(2,2,1)
plot(w,M);grid;ylabel('Modul [dB]')
xlabel('\omega_n');title('Ch. amp-cz. Yule-Walker')
hold on
subplot(2,2,2)
plot(w,Fii);grid;ylabel('Faza [deg]')
xlabel('\omega_n');title('Ch. amp-faz. Yule-Walker')
hold on
hold on
[h1,w1]=freqz(B,A);
M1=20*log(abs(h1));
Fi1=phase(h1); %rad
Fii1=(180/pi)*Fi1;
w1=w1/pi;
subplot(2,2,3)
plot(w1,M1)
ylabel('Modul [dB]')
xlabel('\omega_n');title('Ch. amp-cz. fir2')
grid on
hold on
subplot(2,2,4)
plot(w1,Fii1)
ylabel('Faza [deg]')
xlabel('\omega_n');title('Ch. amp-faz. fir2')
grid on
hold on
