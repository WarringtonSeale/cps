clc
clear all
close all
A=1;
f=1;
fs=1000;
t=0:(1/fs):2;
n=[7,30,99];
w=2*pi*f;
%1 prostokatny bipolarny
C=4*A/pi;
for j=1:3
y=0;
for i=1:2:n(:,j)
y=y+((1/i)*sin(i*w*t));
end
y=y*C;
Y(j,:)=y;
end
figure(1)
subplot(3,1,1)
plot(t,Y(1,:))
subplot(3,1,2)
plot(t,Y(2,:))
subplot(3,1,3)
plot(t,Y(3,:))




%2 prostokatny unipolarny wypelnienie 1/2
C1=A/2;
C2=2*(A/pi);
for j=1:3
x=0;
for i=1:4:n(:,j)
x=x+((1/i)*cos(i*w*t));
end
for i=3:4:n(:,j)
x=x-((1/i)*cos(i*w*t));
end
x=(x*C2)+C1;
X(j,:)=x;
end
figure(2)
subplot(3,1,1)
plot(t,X(1,:))
subplot(3,1,2)
plot(t,X(2,:))
subplot(3,1,3)
plot(t,X(3,:))





%3 prostokatny unipolarny wypelnienie dowolne
T=1/f;
tau=0.1;
for j=1:3
ax=0;
for i=1:1:n(:,j)
ax=ax+sin(pi*i*tau/T)*cos(i*2*pi*f*t)/(pi*i*tau/T);
%
ax=ax+sin(i*pi*tau/T)*cos(i*w*t)/(i*pi*tau/T);
end
ax=A*tau/T+2*A*tau*ax/T;
%
 ax=(2*A*tau*ax/T)+(A*tau/T);
AX(:,j)=ax;
end
figure(3)
subplot(3,1,1)
plot(t,AX(:,1))
subplot(3,1,2)
plot(t,AX(:,2))
subplot(3,1,3)
plot(t,AX(:,3))



%4 trojkatny bipolarny1
C_5=8*A/(pi*pi);
for j=1:3
yy=0;
for i=1:4:n(:,j)
yy=yy+(1/(i^2))*sin(i*w*t);
end
for i=3:4:n(:,j)
yy=yy-(1/(i^2))*sin(i*w*t);
end
YY(j,:)=yy*C_5;
end
figure(4)
subplot(3,1,1)
plot(t,YY(1,:))
subplot(3,1,2)
plot(t,YY(2,:))
subplot(3,1,3)
plot(t,YY(3,:))




%5 trojkatny bipolarny2-piloksztaltny
C_6=2*A/(pi);
for j=1:3
z=0;
for i=1:2:n(:,j)
z=z+(1/(i))*sin(i*w*t);
end
for i=2:2:n(:,j)
z=z-(1/(i))*sin(i*w*t);
end
Z(j,:)=z*C_6;
end
figure(5)
subplot(3,1,1)
plot(t,Z(1,:))
subplot(3,1,2)
plot(t,Z(2,:))
subplot(3,1,3)
plot(t,Z(3,:))


%6 trojkatny unipolarny1
C7=-4*A/(pi*pi);
C8=A/2;
for j=1:3
zz=0;
for i=0:n(:,j)
zz=zz+(1/((2*i+1)^2))*cos((2*i+1)*w*t);
end
ZZ(j,:)=(zz*C7)+C8;
end
figure(6)
subplot(3,1,1)
plot(t,ZZ(1,:))
subplot(3,1,2)
plot(t,ZZ(2,:))
subplot(3,1,3)
plot(t,ZZ(3,:))



%7 trojkatny unipolarny2-piloksztaltny
C9=-A/(pi);
C10=A/2;
for j=1:3
xxx=0;
for i=1:n(:,j)
xxx=xxx+(1/i)*sin(i*w*t);
end
XXX(j,:)=(xxx*C9)+C10;
end
figure(7)
subplot(3,1,1)
plot(t,XXX(1,:))
subplot(3,1,2)
plot(t,XXX(2,:))
subplot(3,1,3)
plot(t,XXX(3,:))



%8 sinusoidalny wyprostowany dwupolowkowy
C11=2*A/(pi);
C12=-4*A/(pi);
for j=1:3
yyy=0;
for i=1:n(:,j)
yyy=yyy+(1/(4*i*i-1))*cos(2*i*w*t);
end
YYY(j,:)=(yyy*C12)+C11;
end
figure(8)
subplot(3,1,1)
plot(t,YYY(1,:))
subplot(3,1,2)
plot(t,YYY(2,:))
subplot(3,1,3)
plot(t,YYY(3,:))


%9 sinusoidalny wyprostowany jednopolowkowy
C13=A/(pi);
C14=A/2;
C15=-2*A/pi;
for j=1:3
zzz=0;
for i=1:n(:,j)
zzz=zzz+(1/(4*i*i-1))*cos(2*i*w*t);
end
Z(j,:)=(zzz*C15)+(sin(w*t)*C14)+C13;
end
figure(9)
subplot(3,1,1)
plot(t,Z(1,:))
subplot(3,1,2)
plot(t,Z(2,:))
subplot(3,1,3)
plot(t,Z(3,:))
