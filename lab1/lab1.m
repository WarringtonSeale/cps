clc; clear; close;

A = 0.7;
f = 100;
omega = 2*pi*f;
fs = 1000;
t_max = 0.1;
t = [0:1/fs:t_max];

y = A*sin(omega*t);

figure(1)
subplot(2, 1, 1)
plot(t, y);
xlabel('t [s]');
ylabel('y(t) [-]');
title('Sygnal wygenerowany')
grid on;

outputfile = fopen('data.csv', 'w');
fprintf(outputfile, 't\ty\n');
fprintf(outputfile, '%f\t%f\n', [t;y]); %% %f - float, %x.yf - float o x 
fclose(outputfile);

clear


inputfile = fopen('/home/juan/repos/cps/lab1/data.csv', 'r');

fgetl(inputfile); % przeczytaj i zapomnij pierwsza linijke (z naglowkami)

data = fscanf(inputfile, '%g\t%g', [2 Inf]);
data = data';

fclose(inputfile);

subplot(2, 1, 2)
plot(data(:,1),data(:,2))
xlabel('t [s]');
ylabel('y(t) [-]');
title('Sygnal odczytany z pliku'); grid on
