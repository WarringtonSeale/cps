clear all;clc;close all;
% Parametry wejsciowe
M=32;
 % polowa dlugosci okna (cale okno N=2M-1)
Nx=128; % dlugosc sygnalu testowego
% Sygnal testowy z modulacja czestotliwosci typu LFM i SFM
fpr=128; f0=0; df=32; fn=16; fm=3; dfm=12; dt=1/fpr; n=0:Nx-1; t=n*dt;
% x = sin(2*pi*(f0*t+0.5*df*t.^2));
x = sin( 2*pi* (fn*t + (dfm/(2*pi*fm))*sin(2*pi*fm*t)) );
% Analiza TF  krotkoczasowa reprezentacja Fouriera
x=hilbert(x);
w = hanning(2*M-1)';
for n = M : Nx-M+1
xx = x(n-(M-1): 1 :n+(M-1)); xx = xx .* w; xx = [ xx 0 ];
X(:,n-M+1) = fftshift(abs(fft(xx))');
end
% Rysunek widma TF
t=t(M:Nx-M+1); f=fpr/(2*M)*(-M:M-1);
figure(1)
subplot(2, 1, 1)
surf(t,f,X); view(-40,70); axis tight;
xlabel('czas [s]'); ylabel('czestotliwosc [Hz]'); 
subplot(2, 1, 2)
imagesc(t,f,X); xlabel('czas [s]'); ylabel('czestotliwosc [Hz]'); 
 