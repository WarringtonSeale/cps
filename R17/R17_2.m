clear all; clf;
niter = 3;
c = 1; d = 0;
% liczba iteracji
% {c=1, d=0}  funkcja skalujaca, {c=0, d=1}  falka
% definicja wspolczynnikow filtrow h0 i h1 systemu falkowego Db4 (17.62) (17.57)
h0 = [ (1+sqrt(3))/(4*sqrt(2)) (3+sqrt(3))/(4*sqrt(2)) ...
(3-sqrt(3))/(4*sqrt(2)) (1-sqrt(3))/(4*sqrt(2)) ];
N = length(h0); n = 0:N-1;
h1 = (-1).^n .* h0(N:-1:1);
% synteza  wedlug schematu drzewa filtrow z rysunku 17.15
c = [ 0 c 0 ];
 % aproksymacje 0
d = [ 0 d 0 ];
 % detale  0
c = conv(c,h0) + conv(d,h1);
for n = 1 : niter
for k = 1:length(c)
c0(2*k-1) = c(k);
c0(2*k) = 0;
end
c0 = [ 0 c0 ];
c = conv(c0,h0);
end
figure(1)
plot(c);

