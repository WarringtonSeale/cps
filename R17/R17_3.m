clear all; close all; clc
%  rametry programu
niter = 3; % liczba iteracji
nx = 2^niter*32; % dlugosc sygnalu

% Definicja wspolczynnikow filtra LP syntezy h0s, np. Db4
h0s = [ (1+sqrt(3))/(4*sqrt(2)) (3+sqrt(3))/(4*sqrt(2)) ...
(3-sqrt(3))/(4*sqrt(2)) (1-sqrt(3))/(4*sqrt(2)) ];

% Oblicz pozostale filtry
N = length(h0s); n = 0:N-1;
h1s = (-1).^n .* h0s(N:-1:1); % filtr HP syntezy
h0a = h0s(N:-1:1); h1a=h1s(N:-1:1); % filtry LP i HP analizy

% Sygnal testowy
 x=sin(2*pi*(1:nx)/32);
%x=rand(1,nx);
% Analiza
cc = x;
for m=1:niter
c0 = conv(cc,h0a); % filtracja LP
d0 = conv(cc,h1a); % filtracja HP
k=N:2:length(d0)-(N-1); kp=1:length(k); ord(m)=length(kp); dd(m,kp) = d0( k );
k=N:2:length(c0)-(N-1); cc=c0( k );
end
% Synteza
c=cc;
for m=niter:-1:1
c0=[]; d0=[];
for k = 1:length(c)
c0(2*k-1)=c(k); c0(2*k)=0;
end
c = conv(c0,h0s); nc=length(c);
for k = 1:ord(m)
d0(2*k-1) = dd(m,k); d0(2*k) = 0;
end
d = conv(d0,h1s); nd=length(d);
c = c(1:nd);
c = c + d;
end
% Rysunki koncowe
figure(1)
subplot(2, 1, 1)
n = 2*(N-1)*niter : length(c)-2*(N-1)*niter+1;
plot(x); title('WE'); 
subplot(2, 1, 2)
plot(n,x(n)-c(n)); title('WE-WY');  
