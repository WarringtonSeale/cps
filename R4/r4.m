% 1. Jesli maksymalna czestotliwosc sygnalu jest dwa razy wieksza niz czetotliwosc probkowania,
% to z sygnalu cyfrowego mozna dokladnie odtworzyc sygnal analogowy
% 2. Przejscie z czestotliwosci fps (wieksza) na fpn (mniejsza) i powrot, czyli odtworzenie synalu
% fps/fpn = K = liczba calkowita ==> fpn=fps/K > 2
clear all; hold off; close all
fx = 1;
fps = 100;
N = 200;
K = 10;
% czestotliwosc sygnalu [Hz]
% stara czestotliwosc probkowania [Hz]
% liczba probek sygnalu sprobkowanego z czestotliwoscia fps (stara)
% ile razy zmniejszyc czestotliwosc probkowania
% Generacja sygnalu sprobkowanego z czestotliwoscia fps
dts = 1/fps;
 % stary okres probkowania
ts = 0 : dts : (N-1)*dts;
xs = sin(2*pi*fx*ts);
figure(1)
subplot(3, 2, 1)
plot(ts,xs,'r',ts,xs,'o'); grid; title('Sygnal sprobkowany - fp STARE');  
% stem(xs,'b'); title('Sygnal sprobkowany - fp STARE');  
% Przejscie z czestotliwosci wiekszej na mniejsza: xs(n) -> xn(n)
fpn = fps/K;
 % nowa czestotliwosc probkowania = zmniejszona stara czestotliwosc
xn = xs( 1 : K : length(xs) );
M = length(xn);
dtn = K*dts;
tn = 0 : dtn : (M-1)*dtn;
subplot(3, 2, 2)
plot(ts,xs,'r',tn,xn,'o'); grid; title('Sygnal sprobkowany - fp NOWE'); 
subplot(3, 2, 3)
plot(tn,xn,'b',tn,xn,'o'); grid; title('Sygnal sprobkowany - fp NOWE'); 
subplot(3, 2, 4)
stem(xn,'b'); title('Sygnal sprobkowany - fp NOWE');  
% Powrot z czestotliwosci mniejszej na wieksza: xn(n) -> y(n)
% Funkcja aproksymujaca
t = -(N-1)*dts : dts : (N-1)*dts;
f = 1/(2*dtn);
fa = sin(2*pi*f*t)./(2*pi*f*t);
fa(N)=1;
tz = [ -fliplr(tn) tn(2:M)];
z = [zeros(1,M-1) 1 zeros(1,M-1)];
% czas trwania funkcji aproksymujacej
% czestotliwosc zer w funkcji aproksymujacej
% funkcja aproksymujaca
% wartosc w zerze (dzielenie 0/0)
%
%
subplot(3, 2, 5)
plot(t,fa,'b',tz,z,'o'); grid; title('Sinc - funkcja aproksymujaca');  
% Aproksymacja
y = zeros(1,N);
ty = 0 : dts : (N-1)*dts;
figure(2)
for k = 1 : M
fa1 = fa( (N)-(k-1)*K : (2*N-1)-(k-1)*K );
y1 = xn(k) * fa1;
y = y + y1;
subplot(311); plot(ty,fa1); grid; title('Kolejna funkcja aproksymujaca');
subplot(312); plot(ty,y1); grid; title('Kolejny skladnik sumy');
subplot(313); plot(ty,y);
 grid; title('Suma');
 
end
figure(1)
subplot(3, 2, 6);
plot(ty,y,'b'); grid; title('Sygnal odtworzony');  
plot(ty,xs(1:N)-y(1:N),'b'); grid; title('Roznica miedzy sygnalami');  

