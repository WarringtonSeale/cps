clear all; clf; subplot(111);
projekt = 1;
filtr = 1;
odpimp = 0;
fpr = 1000;
% 1=filtr dolnoprzepustowy LP, 2=filtr pasmowoprzepustowy BP
% filtracja: 1=funkcja Matlaba, 2=bufory przesuwne, 3=bufory kolowe
% 1=pokaz odpowiedz impulsowa ukladu
% czestotliwosc probkowania w Hz
% Podaj parametry zer i biegunow transmitancji

if (projekt==1)
fz = [ 50 ];
fp = [ 10 ];
Rz = [ 1 ];
Rp = [ 0.95 ];
fmax = 100; df = 0.1;
end

if (projekt==2)
fz = [ 50 100 150 350 400 450 ];
fp = [ 200 250 300 ];
Rz = [ 1 1 1 1 1 1 ];
Rp = [ 0.9 0.65 0.9 ];
fmax = 500; df = 1;
end

% FILTR DOLNOPRZEPUSTOWY
% czestotliwosci zer w Hz
% czestotliwosci biegunow w HZ
% promienie kolejnych zer
% promienie kolejnych biegunow
% parametry obliczanego widma Fouriera
% FILTR PASMOWOPRZEPUSTOWY
% czestotliwosci zer w Hz
% czestotliwosci biegunow w Hz
% promienie kolejnych zer
% promienie kolejnych biegunow
% parametry obliczanego widma Fouriera
% Oblicz zera i bieguny transmitancji
fi_z = 2*pi*(fz/fpr); % katy zer w pasmie zaporowym
fi_p = 2*pi*(fp/fpr); % katy biegunow w pasmie przepuszczania
z = Rz .* exp(j*fi_z); % zera
p = Rp .* exp(j*fi_p); % bieguny
z = [ z conj(z) ]; % dodanie zer sprzezonych
p = [ p conj(p) ]; % dodanie biegunow sprzezonych

% Rysunek - polozenie zer i biegunow
figure(1);subplot(3, 2, [1, 3])
NP = 1000; fi=2*pi*(0:1:NP-1)/NP; s=sin(fi); c=cos(fi);
plot(s,c,'-k',real(z),imag(z),'or',real(p),imag(p),'xb');
title('ZERA i BIEGUNY'); grid; 

% Oblicz wspolczynniki transmitancji {z,p} ---> {b,a}
wzm = 1; [b,a] = zp2tf(z',p',wzm),  
% Charakterystyka czestotliwosciowa {b,a} -> H(f)
f = 0 : df : fmax; w = 2*pi*f; wn= 2*pi*f/fpr;

H = freqz(b,a,wn);
Habs = abs(H); HdB = 20*log10(Habs); Hfa = unwrap(angle(H));
subplot(3, 2, 2)
plot( f, Habs); grid; title('|H(f)|'); xlabel('f [Hz]');  
subplot(3, 2, 4)
plot( f, HdB); grid; title('|H(f)| dB'); xlabel('f [Hz]');  
subplot(3, 2, 5)
plot( f, Hfa); grid; title('kat H(f)'); xlabel('f [Hz]'); ylabel('[rd]');  
% Generacja sygnalow testowych
Nx=1024; n=0:Nx-1; dt=1/fpr; t=dt*n;
f1=10; f2=50; f3=250;
x1=sin(2*pi*f1*t); x2=sin(2*pi*f2*t); x3=sin(2*pi*f3*t);
if (projekt==1) x=x1+x2; else x=x1+x2+x3; end
 dd=zeros(1,Nx); x(1)=1; 
% Filtracja sygnalu: x(n) ---[b,a]---> y(n)
if (filtr==1) y = filter(b,a,x);
    h=filter(b, a, dd) ;
end
if (filtr==2) y = filterBP(b,a,x);
    h = filterBP(b, a, dd) ;
end
if (filtr==3) y = filterBK(b,a,x); 
    h = filterBK(b, a, dd);
end
% funkcja Matlaba
% nasza funkcja wykorzystujaca bufory przesuwne
% nasza funkcja wykorzystujaca bufory kolowe
% Prezentacja wynikow filtracji
%subplot(3, 2, 6); plot(t,dd); grid; axis tight; title('Wejscie x(n)');
figure(2)
subplot(2, 2, 1); plot(t,x); grid; axis tight; title('Wejscie x(n)');
subplot(2, 2, 2); plot(t,y); grid; axis tight; title('Wyjscie y(n)');
xlabel('nr probki n')
n=Nx/2+1:Nx; X = freqz(x(n),1,wn)/(Nx/4); Y = freqz(y(n),1,wn)/(Nx/4);
X = abs(X); Y = abs(Y);

subplot(2, 2, 3); plot(f,X); grid; title('Wejscie X(f)');
subplot(2, 2, 4); plot(f,Y); grid; title('Wyjscie Y(f)');
xlabel('f [Hz]'); 
