function [zz,pp,wzm]=lp2lpTz(z,p,wzm,w0)
%low pass to low pass
zz=[]; pp=[];
for k=1:length(z)
    zz=[zz z(k)*w0];
    wzm=wzm/w0;
end
for k=1:length(p)
    pp=[pp p(k)*w0];
    wzm=wzm*w0;
end