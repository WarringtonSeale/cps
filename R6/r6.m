clc;clear;close all;
nfig1 = 4
mfig1 = 2
%Projektowanie dolnoprzepustowego filtra Butterwortha

fpass = 8000;%Hz
fstop = 22050;%Hz
apass = 2;
astop = 40;

wzm_p = 10^(-apass/20); %obliczanie wzmocnienia odpowiadajacego tlumieniu w dB
wzm_s = 10^(-astop/20);

ws = fstop/fpass; %
vp = 2*pi*fpass; %Rad/s
vs = 2*pi*fstop; %Rad/s

f_ps = [fpass, fstop]
wzm_ps = [wzm_p, wzm_s];
wzmdB_ps = [-apass, -astop];

wp = 1;
Nreal = log10( (10^(astop/10)-1)/(10^(apass/10)-1) / (2*log10(ws/wp))) %obliczamy wymagany rzad filtra
N = ceil(Nreal)
w0 = ws/ (10^(astop/10)-1)^(1/(2*N))

dfi0 = (2*pi) / 2*N;

 for kat= [0:1:N-1]
 fi(kat+1) = pi/2 + dfi0/2 + (kat)*dfi0 %wyznaczamy wekor zawierajacy katy biegunow
 end
 
 p = w0*exp(i*fi)
 z = []
 wzm = real(prod(-p))

 figure(1)
 subplot(nfig1, mfig1, 1)
 plot(real(p), imag(p), 'xb');grid;title('bieguny filtra prototypowego');xlabel('Re');ylabel('Im');
 
 b = wzm;
 a = poly(p)
 
 [N_matlab, w0_matlab] = buttord(vp, vs, apass, astop, 's')
 diff = N_matlab-N
 
 w  =0:0.005:2;
 H = freqs(b, a, w);
 subplot(nfig1, mfig1, 2)
 plot(w, abs(H));title('modul prototypu');xlabel('\omega[rad/s]');grid;ylabel('modul');
 subplot(nfig1, mfig1, 3)
 plot(w, 20*log10(abs(H)));title('modul prototypu w dB');xlabel('\omega[rad/s]');grid;ylabel('modul [dB]')
 
 [z, p, wzm] = lp2lpTz(z, p, wzm,vp) %Transformata liniowa czestotliwosci
 b = wzm*poly(z); a = poly(p)
 
 subplot(nfig1, mfig1, 4)
 plot(real(z), imag(z), 'or', real(p), imag(p), 'xb');title('bieguny i zera po transformacie');xlabel('Re');ylabel('Im');grid
 
 %Filtr docelowy
 z, p, a, b
 
 NF = 10000;
 fmin=0;
 fmax=50000;%Hz
 
 f = [fmin:(fmax-fmin)/(NF-1):fmax];
 w = 2*pi*f;
 H = freqs(b, a, w);
 subplot(nfig1, mfig1, 5)
 plot(f, abs(H));hold on;plot(f_ps, wzm_ps, 'ro');xlabel('\omega[rad/s]');ylabel('modul[-]');title('modul gotowego filtru');grid
 subplot(nfig1, mfig1, 6)
 plot(f, 20*log10(abs(H)), f_ps, wzmdB_ps, 'ro');xlabel('\omega[rad/s]');ylabel('modul[dB]');title('modul gotowego filtru w db');grid
subplot(nfig1, mfig1, 7)
plot(f, unwrap(angle(H)));title('faza');xlabel('\omega[rad/s]');ylabel('\phi[rad]');grid

%wyznaczanie parametrow elementow ukladu
