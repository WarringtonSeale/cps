function [zz,pp,wzm] = lp2bsPZ(z,p,wzm,wc,dw) 
% LowPass to BandStop TZ 
 
zz = []; pp = []; 
for  k=1:length(z) 
     zz = [ zz roots([ 1 -dw/z(k) wc^2 ])' ]; 
     wzm = wzm*(-z(k)); 
end 
for  k=1:length(p) 
     pp = [ pp roots([ 1 -dw/p(k) wc^2 ])' ]; 
     wzm = wzm/(-p(k)); 
end 
 
if (length(p)>length(z)) 
  for k=1:(length(p)-length(z)) 
      zz = [ zz roots([ 1 0 wc^2 ])' ]; 
  end 
end 
 
if (length(z)>length(p)) 
  for k=1:(length(z)-length(p)) 
      pp = [ pp roots([ 1 0 wc^2 ])' ]; 
  end 
end 