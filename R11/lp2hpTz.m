function [zz,pp,wzm] = lp2hpTZ(z,p,wzm,wp) 
% LowPass to HighPass TZ 
 
zz = []; pp = []; 
for  k=1:length(z) 
     zz = [ zz wp/z(k) ]; 
     wzm = wzm*(-z(k)); 
end 
for  k=1:length(p) 
     pp = [ pp wp/p(k) ]; 
     wzm = wzm/(-p(k)); 
end 
for k=1:(length(p)-length(z)) 
     zz = [ zz 0 ]; 
end    