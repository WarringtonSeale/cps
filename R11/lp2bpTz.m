function [zz,pp,wzm] = lp2bpTZ(z,p,wzm,wc,dw) 
% LowPass to BandPass TZ 
 
pp = []; zz = []; 
for  k=1:length(z) 
     zz = [ zz roots([ 1 -z(k)*dw wc^2])' ]; 
     wzm = wzm/dw; 
end 
for  k=1:length(p) 
     pp = [ pp roots([ 1 -p(k)*dw wc^2])' ]; 
     wzm = wzm*dw; 
end 
for k=1:(length(p)-length(z)) 
     zz = [ zz 0 ]; 
end    