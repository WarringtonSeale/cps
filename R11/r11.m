clear all; subplot(111); clc; close all;
nf1 = 4;
mf1 = 3;
typ = 1;
apass = 3;
astop = 60;
fpr = 2000;
fmx = 1000;


if typ==1 %lowpass
    fpass = 200;
    fstop = 300;
    fpass = 2*fpr*tan(pi*fpass/fpr)/(2*pi);
    fstop = 2*fpr*tan(pi*fstop/fpr)/(2*pi);
    ws = fstop/fpass;
end

if (typ==2) %highpass
    fstop = 700;
    fpass = 800;
    fpass = 2*fpr*tan(pi*fpass/fpr)/(2*pi);
    fstop = 2*fpr*tan(pi*fstop/fpr)/(2*pi);
    ws = fpass/fstop;
end

if (typ==3) %bandpass
fs1 = 300;
fp1 = 400;
fp2 = 600;
fs2 = 700;
fp1 = 2*fpr*tan(pi*fp1/fpr)/(2*pi);
fp2 = 2*fpr*tan(pi*fp2/fpr)/(2*pi);
fs1 = 2*fpr*tan(pi*fs1/fpr)/(2*pi);
fs2 = 2*fpr*tan(pi*fs2/fpr)/(2*pi);
ws1t = (fs1^2 - fp1*fp2) / (fs1*(fp2-fp1));
ws2t = (fs2^2 - fp1*fp2) / (fs2*(fp2-fp1));
ws = min( abs(ws1t), abs(ws2t) );
end

if (typ==4) %bandstop
fp1 = 200;
fs1 = 300;
fs2 = 700;
fp2 = 800;
fp1 = 2*fpr*tan(pi*fp1/fpr)/(2*pi); 
fp2 = 2*fpr*tan(pi*fp2/fpr)/(2*pi); 
fs1 = 2*fpr*tan(pi*fs1/fpr)/(2*pi); 
fs2 = 2*fpr*tan(pi*fs2/fpr)/(2*pi); 
ws1t = (fs1*(fp2-fp1)) / (fs1^2 - fp1*fp2);
ws2t = (fs2*(fp2-fp1)) / (fs2^2 - fp1*fp2);
ws = min( abs(ws1t), abs(ws2t) );
end

wzm_p = 10^(-apass/20);
wzm_s = 10^(-astop/20); %z decybeli na wspolczynnik

if( (typ==1) | (typ==2) )
vp = 2*pi*fpass;
vs = 2*pi*fstop;
f_ps = [fpass, fstop]; wzm_ps = [wzm_p, wzm_s]; wzmdB_ps = [-apass, -astop];
end
if( (typ==3) | (typ==4) )
vp = 2*pi*[ fp1 fp2 ];
vs = 2*pi*[ fs1 fs2 ];
vc = 2*pi*sqrt(fp1*fp2); % pulsacja srodka
dv = 2*pi*(fp2-fp1); % szerokosc filtra wokol vc
f_ps = [fp1,fp2,fs1,fs2];
wzm_ps = [wzm_p, wzm_p, wzm_s, wzm_s];
wzmdB_ps = [-apass, -apass, -astop, -astop];
end

wp = 1; %rzad i omega0
N = ceil( log10( (10^(astop/10)-1) / (10^(apass/10)-1) ) / (2*log10(ws/wp)) )
w0 = ws / (10^(astop/10)-1)^(1/(2*N))

dfi0 = (2*pi)/(2*N); % kat kawalka tortu
fi = pi/2 + dfi0/2 + (0 : N-1)*dfi0; % katy biegunow
p = w0*exp(j*fi); % bieguny
z = []; % zera
wzm = prod(-p); % wzmocnienie
a = poly(p); % bieguny --> wsp wielomianu mianownika A(z)
b = wzm; % wielomian licznika B(z)

subplot(nf1, mf1, 1)

plot( real(p), imag(p), 'x' ); grid; title('Polozenie biegunow');
xlabel('real'); ylabel('imag');

[NN,ww0] = buttord( vp, vs, apass, astop, 's' );
blad_N = N-NN


w = 0 : 0.005 : 2; % zakres pulsacji unormowanej; pulsacja granicy pasma przepustowego = 1
H = freqs(b,a,w); % alternatywa: H = polyval( b,j*w)./polyval(a,j*w);
subplot(nf1, mf1, 2)
plot(w,abs(H)); grid; title('Modul prototypu LPass'); xlabel('pulsacja [rad/sek]');
subplot(nf1, mf1, 3)
plot(w,20*log10(abs(H))); grid; title('Modul prototypu LowPass w dB');
xlabel('pulsacja [rad/sek]'); ylabel('dB')

if (typ==1) 
    [z,p,wzm] = lp2lpTz(z,p,wzm,vp);
end % LowPass to LowPass: s=s/w0
if (typ==2) 
    [z,p,wzm] = lp2hpTz(z,p,wzm,vp); 
end % LowPass to HighPass: s=w0/s
if (typ==3) 
    [z,p,wzm] = lp2bpTz(z,p,wzm,vc,dv); 
end % LowPass to BandPass: s=(s^2+wc^2)/(dw*s)
if (typ==4) 
    [z,p,wzm] = lp2bsTz(z,p,wzm,vc,dv); 
end % LowPass to BandStop: s=(dw*s)/(s^2+wc^2)

b=wzm*poly(z); a=poly(p);
subplot(nf1, mf1, 4)
plot( real(z), imag(z), 'o',real(p),imag(p),'x' ); grid;
title('Polozenie biegunow'); xlabel('real'); ylabel('imag');  
p, z,  
a, b,  
printsys(b,a,'s');

NF = 1000; % ile punktow
fmin = 0; % dolna czestotliwosc
fmax = 5000; % gorna czestotliwosc
f = fmin : (fmax-fmin)/(NF-1) : fmax; % wyznaczane czestotliwosci
w = 2*pi*f; % wyznaczane pulsacje
H = freqs(b,a,w); % alternatywa: H = polyval( b,j*w)./polyval(a,j*w);
subplot(nf1, mf1, 5)
plot( f,abs(H), f_ps,wzm_ps,'ro');
grid; title('Modul'); xlabel('freq [Hz]');
subplot(nf1, mf1, 6)
plot(f,20*log10(abs(H)), f_ps,wzmdB_ps,'ro'); axis([fmin,fmax,-100,20]);
grid; title('Modul dB'); xlabel('freq [Hz]'); ylabel('dB');  
subplot(nf1, mf1, 7)
plot(f,unwrap(angle(H))); grid; title('FAZA'); xlabel('freq [Hz]'); ylabel('[rad]');
 

% H(s) --> H(z) Transformacja biliniowa
[zc,pc,wzmc]=bilinearTZ(z,p,wzm,fpr)
bc=wzmc*poly(zc); ac=poly(pc);

% Pokaz zera i bieguny filtra cyfrowego
NP = 1000; fi=2*pi*(0:1:NP-1)/NP; x=sin(fi); y=cos(fi);
subplot(nf1, mf1, 8)
plot(x,y,'-k',real(zc),imag(zc),'or',real(pc),imag(pc),'xb');
title('ZERA i BIEGUNY filtra cyfrowego'); grid;

% Otrzymana charakterystyka czestotliwosciowa
NF = 1000; fmin = 0; fmax = fmx; f = fmin : (fmax-fmin)/(NF-1) : fmax;
w = 2*pi*f/fpr; H = freqz(bc,ac,w);
Habs=abs(H); HdB=20*log10(Habs); Hfa=unwrap(angle(H));
f_ps = (fpr/pi)*atan(pi*f_ps/fpr);
subplot(nf1, mf1, 9)
plot(f,Habs,f_ps,wzm_ps,'o'); grid; title('Modul'); xlabel('freq [Hz]');  
subplot(nf1, mf1, 10)
plot(f,HdB,f_ps,wzmdB_ps,'o'); grid; title('Modul dB'); xlabel('freq [Hz]');  
subplot(nf1, mf1, 11)
plot(f,Hfa); grid; title('FAZA'); xlabel('freq [Hz]'); ylabel('[rad]');  

% Odpowiedz impulsowa (filtracja sygnalu delty Kroneckera) s
Nx=200; x = zeros(1,Nx); x(1)=1;
M=length(bc); N=length(ac);
ac=ac(2:N); N=N-1;
bx=zeros(1,M); by=zeros(1,N); y=[];
for n=1:Nx
bx = [ x(n) bx(1:M-1)];
y(n) = sum(bx .* bc) - sum(by .* ac);
by = [ y(n) by(1:N-1) ];
end
n=0:Nx-1; plot(n,y); grid; title('Odp impulsowa h(n)'); xlabel('n');  









