function [zz,pp,wzm] = lp2lpTZ(z,p,wzm,wp) 
% LowPass to LowPass TZ 
 
zz = []; pp = []; 
for  k=1:length(z) 
     zz = [ zz z(k)*wp ]; 
     wzm = wzm/wp; 
end 
for  k=1:length(p) 
     pp = [ pp p(k)*wp ]; 
     wzm = wzm*wp; 
end 