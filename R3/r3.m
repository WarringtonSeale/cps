clear all; clf; subplot(111);
% Parametry programu
T=1; % okres przebiegu prostokatnego [sekundy]
N=1000; % liczba probek przypadajacych na ten okres
dt=T/N; % odleglosc miedzy probkami
t=0:dt:(N-1)*dt; % kolejne chwile czasowe odpowiadajace probkom sygnalu
A=1; % amplituda sygnalu [A, +A]
NF=30; % liczba wspolczynnikow szeregu Fouriera do wyznaczenia (mniejsza lub rowna N/2)

isygnal=1; % numer sygnalu testowego
f0=1/T; % czestotliwosc podstawowa szeregu Fouriera
% Wygenerowanie jednego okresu sygnalu
if (isygnal==1)
 x=[0 A*ones(1,N/2-1) 0 -A*ones(1,N/2-1)]; end
if (isygnal==2)
 x=[A*ones(1,N/4) 0 -A*ones(1,N/2-1) 0 A*ones(1,N/4-1)]; end
if (isygnal==3)
 x=[A*ones(1,N/8) 0 -A*ones(1,5*N/8-1) 0 A*ones(1,2*N/8-1)]; end
if (isygnal==4)
 x=(A/T)*t; end
if (isygnal==5)
 x=[(2*A/T)*t(1:N/2+1) (2*A/T)*t(N/2:-1:2)]; end
if (isygnal==6)
 x=sin(2*pi*t/(T)); end
% Dodaj inne sygnaly ...
% if (isygnal==7) x=?; end
figure(1)
plot(t,x); grid; title('Sygnal analizowany'); xlabel('czas [sek]');  
% Wyznaczenie wspolczynnikow rozwiniecia sygnalu w szereg Fouriera
for k=0:NF-1
ck=cos(2*pi*k*f0*t); % plot(t,ck); grid; title('cosinus');   % k-ty cosinus
sk=sin(2*pi*k*f0*t); % plot(t,sk); grid; title('sinus');  
 % k-ty sinus
a(k+1)=sum(x.*ck)/N; % wspolczynnik kosinusa
b(k+1)=sum(x.*sk)/N; % wspolczynnik sinusa
% F(k+1,1:N)=(ck-j*sk)/sqrt(N);
 % w celach testowych dla malego N
end
% I = F * F'
 % test ortonormalnosci bazy Fouriera dla malego N
%  
figure(2)
f=0 : f0 : (NF-1)*f0;
subplot(211); stem(f,a,'filled'); xlabel('[Hz]'); title('Wspolczynniki cos');
subplot(212); stem(f,b,'filled'); xlabel('[Hz]'); title('Wspolczynniki sin');
 
% Porownanie ze wspolczynnikami teoretycznymi  dodaj dla pozostalych
if (isygnal==1) % funkcja asymetryczna (tylko sinusy)
at=[]; bt=[];
for k=1:2:NF
at=[at 0 0];
bt=[bt 0 (2*A)/(pi*k)];
end
figure(3)
subplot(211); plot(f,a-at(1:NF)); grid; xlabel('[Hz]'); title('Roznica cos');
subplot(212); plot(f,b-bt(1:NF)); grid; xlabel('[Hz]'); title('Roznica sin');
 
end
% Porownanie z dyskretna transformacja Fouriera
X = fft(x,N)/N;
X = conj(X);
% funkcje bazowe exp(-jwt) a nie exp(j*wt),
% czyli dla sygnalow rzeczywistych x mamy sprzezenie
figure(4)
subplot(211); plot( f, a-real(X(1:NF)) ); grid; title('Roznica z DFT - COS');
subplot(212); plot( f, b-imag(X(1:NF)) ); grid; title('Roznica z DFT - SIN');
 figure(5)
% Synteza sygnalu ze wspolczynnikow rozwiniecia
subplot(111);
a(1)=a(1)/2; y=zeros(1,N);
for k=0:NF-1
y = y + 2*a(k+1)*cos(k*2*pi*f0*t) + 2*b(k+1)*sin(k*2*pi*f0*t);
plot(t,y); grid; title('Suma = k pierwszych funkcji bazowych');  
end
figure(6)
subplot(2, 1, 1)
plot(t,y); grid; title('Calkowity sygnal zsyntezowany'); xlabel('czas [sek]');  
subplot(2, 1, 2)
plot(t,y-x); grid; title('Sygnal bledu'); xlabel('czas [sek]');  
