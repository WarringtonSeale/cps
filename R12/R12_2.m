clear all; close all; clc

L = 20;
Nr = 5;
wp = 1; ws = 1;
R = 200;
tol = 10^(-8);
ifigs = 1;
% liczba poszukiwanych wspolczynnikow filtra (parzysta): dlugosc filtra N=2L1
% szerokosc pasma przepustowego 0 < Nr < L
% wagi poszczegolnych podpasm: Pass, Stop
% ile razy zior testowy ma byc wiekszy od zbioru ekstremow
% tolerancja rozwiazania
% wyswietlanie rysunkow podczas iteracji: 0=nie, 1=tak

 M = L+1; % liczba czestotliwosci ekstremow
K = 1+R*(M-1); % liczba wszystkich badanych czestotliwosci
fz = (0 : K-1)/(K-1); % K-elementowy zbior czestotliwosci przeszukiwanych
k11 = 1; k12 = 1+Nr*R; % granice pasma przepustowego
k21 = 1+(Nr+1)*R; k22=K; % granice pasma zaporowego
K1 = 1+Nr*R+R/2; % nr probki charakterystyki dla czestotliwosci granicznej
fd = [ fz(1:K1) fz(K1:K)]; % czestotliwosci charakterystyczne filtra
Hd = [ ones(1,K1) zeros(1,K-K1)]; % wymagane wartosci wzmocnienia
Wg = [ wp*ones(1,K1) ws*ones(1,K-K1)]; % wagi
imax = 1:R:K; % indeksy startowych czestotliwosci ekstremow
% Wybranie startowego zbioru czestotliwosci ekstremow
feMAX = fz(1:R:K);
sigmaMAX = 10^15; sigma = 0;

% Petla glowna
n = 0 : L-1;
while ( (sigmaMAX-sigma) > tol )
sigmaMAX - sigma % pokaz aktualne zmniejszenie wartosci bledu ch-ki
H = Hd(imax); H=H';
W = Wg(imax);% Uaktualnij parametry zmieniane iteracyjnie
fe = feMAX; % nowe czestotliwosci ekstremow w obecnym kroku

% Oblicz macierz kosinusow A
A = [];
for m = 0 : M-1 % po czestotliwosciach ekstremow
A = [ A; cos(pi*fe(m+1)*n) ((-1)^m)/W(m+1) ];
end
% Rozwiaz rownanie
% c = A\H';
 % Metoda 1
c = pinv(A)*H;
 % Metoda 2
 
% Pokaz aktualna odpowiedz impulsowa
h = c(1:L); sigma=c(M); sigma=abs(sigma);
g=h'/2; g(1)=2*g(1); g = [ fliplr(g(2:L)) g];
if (ifigs==1)% stem(h); title('Polowa odp impulsowej h');  
    figure(1)
stem(g); title('Cala odp impulsowa h');  ;
end

% Oblicz i pokaz aktualna charakterystyke czestotliwosciowa oraz jej blad
for k = 0 : K-1
H(k+1) = cos(pi*fz(k+1)*n) * h;
Herr(k+1) = Wg(k+1) * (H(k+1) - Hd(k+1));
end
if (ifigs==1)
    figure(2)
    subplot(2, 1, 1)
plot(fz,Hd,'r',fz,H,'b'); grid; title('Aktualna H(f)');  
subplot(2, 1, 2)
plot(fz,Herr); grid; title('Blad H(f)');  
end

% Znajdz M+1 najwiekszych ekstremow funkcji bledu (czestotliwosc, wartosc ekstremum)
% tzn. oblicz feMAX i sigmaMAX
% Znajdz wszystkie ekstrema
Hmax = []; imax = [];
for p = 1 : 2
 % kolejne przedzialy: przepuszczania i zaporowy
if (p==1) k1=k11; k2=k12; end % pasmo przepustowe
if (p==2) k1=k21; k2=k22; end % pasmo zaporowe
Hmax = [ Hmax Herr(k1)]; imax = [ imax k1 ]; % zapisz pierwszy element / indeks
k=k1+1; % zwieksz indeks
while( Herr(k-1) == Herr(k) ) k = k+1; end % inkrementuj indeks, jesli rowne
if ( Herr(k) < Herr(k+1) ) %
sgn=1; % charakterystyka narasta
else
sgn=-1; % charakterystyka opada
end
 
k=k+1;
 while ( k <= k2 ) % poluj na kolejne ekstrema
if (sgn==1)
while( (k<k2) & (Herr(k-1)<Herr(k)) ) k=k+1; end
end
if (sgn==-1)
while( (k<k2) & (Herr(k-1)>Herr(k)) ) k=k+1; end
end
sgn = -sgn;
Hmax = [ Hmax Herr(k) ]; imax = [imax k]; % zapamietaj kolejne ekstremum
k=k+1;
end % end while
end % end for

if (ifigs==1);figure(3)
plot(fz(imax),Hmax,'or',fz,Herr,'b');
grid; title('Blad charakterystyki i jego ekstrema');  
end% Wybierz M+1 najwiekszych
if ( length(Hmax)>M )
disp('UWAGA!!! Wiecej EKSTREMOW niz M+1!');
IM = []; G = abs(Hmax); LenG = length(G);
while( LenG > 0 )
Gmx = max(G); imx=find(G==Gmx);
LenGmx = length(imx);
IM = [ IM imax(imx)];
G(imx)=0; LenG = LenG-LenGmx;
end
IM = IM(1:M); IM = sort(IM); imax = IM;
end
sigmaMAX = max( abs(Hmax) );
feMAX = fz( imax );
if (ifigs==1)
    figure(4)
plot(fz(imax),Herr(imax),'or',fz,Herr,'b');
grid; title('Blad charakterystyki i M+1 najwiekszych ekstremow');  
end
end % end while(tol)

% Rysunki
fz=fz/2;
figure(5)
subplot(2, 2, 1)
stem(g); title('Wynikowa odp impulsowa filtra'); 
subplot(2, 2, 2)
plot(fz(imax),Herr(imax),'or',fz,Herr,'b'); grid;
title('Blad H(f) + jego EKSTREMA');  
subplot(2, 2,3)
plot(fz,Hd,'r',fz,H,'b'); grid; title('Wynikowe H(f)');  
subplot(2, 2, 4)
plot(fz,20*log10(H),'b'); grid; title('Wynikowe H(f) w dB');  

