clear all;
M = 20;9
typ = 1;
% polowa dlugosci filtra (caly N=2*M+1)
% 1 = Hilberta, 2 = rozniczkujacy, 3 = interpolujacy (K-krotnie)
N=2*M+1; n = 1 : M;% Wygeneruj teoretyczne odpowiedzi impulsowe
if (typ==1) h = (2/pi) * sin(pi*n/2).^2 ./ n; end % polowa odpowiedzi impulsowej
if (typ==2) h = cos(pi*n)./n; end % dla n = 1, 2, 3, ..., M
if (typ==3) %
K=5; wc=pi/K; fc=wc/(2*pi); % fc = 1/(2*K)
h = 2*fc*sin(wc*n)./(wc*n); % wspolczynnik skalujacy 2*fc = 1/K
end
if (typ==1 | typ==2) % cala odpowiedz dla n = M,...,0,...,M
h = [ -h(M:-1:1) 0 h(1:M) ];
else
h = K*[ h(M:-1:1) 2*fc h(1:M)]; % wzmocnienie K-razy, aby probka srodkowa byla rowna 1
end

% Wymnoz je z funkcja okna
w = blackman(N); w=w';
hw = h .* w;% okno Blackmana

% wymnozenie odpowiedzi impulsowej z oknem

% Oblicz widmo Fouriera
m = -M : 1 : M; % dla filtra nieprzyczynowego (bez przesuniecia o M probek w prawo)
% m = 0 : N-1; % dla filtra przyczynowego (z przesunieciem o M probek w prawo)
NF=500; fn=0.5*(1:NF-1)/NF;

for k=1:NF-1
H(k)=sum( h .* exp(-j*2*pi*fn(k)*m) );
HW(k)=sum( hw .* exp(-j*2*pi*fn(k)*m) );
end

% Rysunki
figure(1)
subplot(3, 2, 1)
stem(m,h); grid; title('h(n)'); xlabel('n'); 
subplot(3, 2, 2)
stem(m,hw); grid; title('hw(n)'); xlabel('n'); 
subplot(3, 2, 3)
plot(fn,abs(H)); grid; title('|H(fn)|'); xlabel('f norm]'); 
subplot(3, 2, 4)
plot(fn,abs(HW)); grid; title('|HW(fn)|'); xlabel('f norm]'); 
subplot(3, 2, 5)
plot(fn,unwrap(angle(H))); grid; title('kat H(fn) [rd]'); xlabel('f norm'); 
subplot(3, 2, 6)
plot(fn,unwrap(angle(HW))); grid; title('kat HW(fn) [rd]'); xlabel('f norm'); 
figure(2)
subplot(3, 1, 1)
% Zastosowanie filtra Hilberta i filtra rozniczkujacego
if(typ==1 | typ==2)
Nx=200; fx=50; fpr=1000; n=0:Nx-1; x=cos(2*pi*fx/fpr*n); % generacja sygnalu testowego x(n)
y=conv(x,hw);
 % filtracja sygnalu x(n) za pomoca odp. impulsowej hw(n); otrzymujemy Nx+N1 probek
yp=y(N:Nx);
 % odciecie stanow przejsciowych (po N1 probek) z przodu i z tylu sygnalu y(n)
xp=x(M+1:Nx-M); % odciecie tych probek z x(n), dla ktorych nie ma poprawnych odpowiednikow w y(n)
if(typ==1) % filtr Hilberta
z = xp + j*yp;
 % sygnal analityczny

Ny=ceil(fpr/fx); k=1:Ny; plot(k,xp(k),'b',k,yp(k),'r');
title('xp(n) i yp(n)'); grid;   % -90 stopni
subplot(3, 1, 2)
plot(xp,yp); title('Cz. urojona w funkcji cz. rzeczywistej');
grid;  
 % powinien byc okrag
 subplot(3, 1, 3)
plot(abs(fft(z))); title('Widmo sygnalu analitycznego');
grid;  
 % brak czestotliwosci ujemnych
else
 % filtr rozniczkujacy

Ny=ceil(fpr/fx); k=1:Ny; plot(k,xp(k),'b',k,yp(k),'r'); title('xp(n) i yp(n)');
grid;  
 % przesuniecie w fazie +90 stopni
end
end
% Zastosowanie filtra interpolujacego
if(typ==3)
Nx=50; fx=50; fpr=1000; n=0:Nx-1; x=cos(2*pi*fx/fpr*n); % generacja sygnalu testowego x(n)
xz=[]; KNx=K*Nx; xz=zeros(1,KNx); xz(1:K:KNx)=x(1:Nx); % dodanie zer
yz=conv(xz,hw);
 % filtracja xz(n) za pomoca odp. impulsowej hw(n); otrzymujemy Nx+N1 probek
yp=yz(N:KNx);
 % odciecie stanow przejsciowych (po N1 probek) z przodu i z tylu sygnalu yz(n)
xp=xz(M+1:KNx-M); % odciecie tych probek w xz(n), dla ktorych nie ma poprawnych odpowiednikow w yz(n)
Ny=length(yp); k=1:Ny; plot(k,xp(k),'or',k,yp(k),'-b');

title('xp(n) i yp(n)'); grid;   % porownanie
end
 
