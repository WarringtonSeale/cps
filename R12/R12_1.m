21% Projektowanie filtrow FIR metoda WAZONEJ minimalizacji bledu sredniokwadratowego
% pomiedzy zadana charakterystyka, sprobkowana w dziedzinie czestotliwosci, a charakterystyka otrzymywana
clear all; close all; clc;
% Podaj swoje wymagania: K punktow charakterystyki czestotliwosciowej Ak, k = 0, 1, 2, ..., K1
M = 20; % polowa dlugosci filtra; calosc N=2M+1
K = 50; % liczba punktow charakterystyki czestotliwosciowej (parzysta; K=>2*M)
% Podaj wymagania czestotliwosciowe, czyli Ak w pulsacjach 2*pi*k/K,
k=0,1,2,...,K-1;
    
L1 = floor(K/4); % liczba jedynek na poczatku, potem punkty przejsciowe, nastepnie zera
Ak = [ ones(1,L1) 0.75 0.25 zeros(1,K-(2*L1-1)-4) 0.25 0.75 ones(1,L1-1)];
Ak = Ak';

% Podaj wspolczynniki funkcji wagowej w(k), k = 0, 1, 2,..., K1, odpowiadajace zadanym
% punktom charakterystyki czestotliwosciowej

wp = 1; % waga odcinka PassBand
wt = 1; % waga odcinka TransientBand
ws = 1; % waga odcinka StopBand
w = [ wp*ones(1,L1) wt wt ws*ones(1,K-(2*L1-1)-4) wt wt wp*ones(1,L1-1) ];
W = zeros(K,K); % macierz diagonalna z wagami

for k=1:K
 % na glownej przekatnej
W(k,k)=w(k); 
end

 
% Wznacz macierz F rownania macierzowego W*F*h = W*(Ak + err)
F = [];
n = 0 : M-1;
for k = 0 : K-1
F = [ F; 2*cos(2*pi*(M-n)*k/K) 1 ];
end


% Poniewaz dla h minimalizujacego blad W*F*h = W*Ak, stad
h = pinv(W*F)*(W*Ak); % metoda 1
% h = (W*F)\(W*Ak); % metoda 2
 h = [ h; h(M:-1:1) ];
 
% Rysunki
figure(1)
n = 0 : 2*M; subplot(2, 2, 1); stem(n,h); grid;
title('Odp impulsowa filtra'); xlabel('nr probki'); 

NF=500; wn =0:pi/(NF-1):pi; fn = wn/(2*pi); H = freqz(h,1,wn);
subplot(2, 2, 2); plot(fn,abs(H)); grid; title('Modul odp czestotliwosciowej');
subplot(2, 2, 3); plot(fn,180/pi*unwrap(angle(H))); grid;
title('Faza odp czestotliwosciowej'); ylabel('stopnie'); xlabel('f norm [Hz]'); 
subplot(2, 2, 4); plot(fn,20*log10(abs(H))); grid; title('Modul odp czestotliwosciowej');
xlabel('f norm [Hz]'); axis([0 0.5 -100 10]); 

