clc;clear all;close all;
% Podaj parametry filtra (np. pasmowozaporowego)
fpr = 1000;
 % czestotliwosc probkowania [Hz]
fd1 = 150;
 % czestotliwosc dolna 1 [Hz]
fd2 = 200;
 % czestotliwosc dolna 2 [Hz]
fg1 = 300;
 % czestotliwosc gorna 1 [Hz]
fg2 = 350;
 % czestotliwosc gorna 2 [Hz] 
dp = 0.001;
 % oscylacje w pasmie przepustowym np. 0.1, 0.01, 0.001
ds = 0.0001;
 % oscylacje w pasmie zaporowym np. 0.001, 0.001, 0.0001
typ = 'bs';
 % lp=LowPass, hp=HighPass, bp=BandPass, bs=BandStop
% Oblicz parametry okna
if (typ=='lp')
 % Filtr LP
df=fd2-fd1; fc=((fd1+fd2)/2)/fpr; wc=2*pi*fc;
end
if (typ=='hp')
 % Filtr HP
df=fg2-fg1; fc=((fg1+fg2)/2)/fpr; wc=2*pi*fc; end
if (typ=='bp' | typ=='bs') % Filtr BP lub BS
df1=fd2-fd1; df2=fg2-fg1; df=min(df1,df2);
f1=(fd1+(df/2))/fpr; f2=(fg2-(df/2))/fpr; w1=2*pi*f1; w2=2*pi*f2;
end
% dp=((10^(Ap/20))-1)/((10^(Ap/20))+1); % opcjonalne przeliczenie Ap i As w [dB]
% ds=10^(-As/20);
 % na dp i ds
d=min(dp,ds); A=-20*log10(d);
if (A>=50) beta=0.1102*(A-8.7); end
if (A>21 & A<50) beta=(0.5842*(A-21)^0.4)+0.07886*(A-21); end
if (A<=21) beta=0; end
if (A>21) D=(A-7.95)/14.36; end
if (A<=21) D=0.922; end
N=(D*fpr/df)+1; N=ceil(N); if(rem(N,2)==0) N=N+1; end
N

M = (N-1)/2; m = 1 : M; n = 1 : N;
% Wygeneruj okno
w = besseli( 0, beta * sqrt(1-((n-1)-M).^2/M^2) ) / besseli(0,beta);
figure(1);subplot(3, 2, 1)
plot(n,w); title('Funkcja okna'); grid; 
% Wygeneruj odpowiedz impulsowa filtra
if (typ=='lp') h=2*fc*sin(wc*m)./(wc*m); h=[ fliplr(h) 2*fc h]; end
if (typ=='hp') h=-2*fc*sin(wc*m)./(wc*m); h=[ fliplr(h) 1-2*fc h]; end
if (typ=='bp') % filtr BP
h = 2*f2*sin(w2*m)./(w2*m) - 2*f1*sin(w1*m)./(w1*m);
h = [ fliplr(h) 2*(f2-f1) h];
end
if (typ=='bs') % filtr BS
h = 2*f1*sin(w1*m)./(w1*m) - 2*f2*sin(w2*m)./(w2*m);
h = [ fliplr(h) 1+2*(f1-f2) h];
end
subplot(3, 2, 2)
plot(n,h); title('Odp impulsowa filtra'); grid; 
% Wymnoz odp impulsowa filtra z funkcja okna
hw = h.* w;
subplot(3, 2, 3)
plot(n,hw,'b'); title('Odpowiedz impulsowa filtra z oknem'); grid;  
% Charakterystyka czestotliwosciowa
NF = 1000; fmin = 0; fmax = fpr/2;
 % wartosci parametrow charakterystyki
f = fmin : (fmax-fmin)/(NF-1) : fmax; % czestotliwosc
w = 2*pi*f/fpr;
 % pulsacja
HW = freqz(hw,1,w);
 % widmo Fouriera
 subplot(3, 2, 4)
plot(f,abs(HW)); grid; title('Modul'); xlabel('freq [Hz]'); 
subplot(3, 2, 5)
plot(f,20*log10(abs(HW))); grid; title('Modul dB'); xlabel('[Hz]'); ylabel('dB');
subplot(3, 2, 6)
plot(f,unwrap(angle(HW))); grid; title('FAZA'); xlabel('[Hz]'); ylabel('[rad]');% filtr LP
% filtr HP

