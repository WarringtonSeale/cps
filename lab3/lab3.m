clc;clear;close all;
% zapisanie sygnalu do .wav, modulacja
%========parametry
A=0.5;
B=-0.3;
f1=1;
fs=10000;
%========wektor czasu
t=0:(1/fs):1;
%======== generowanie sygnalu
y1=A*sin(2*pi*f1*t);
%=========zapis do wav
audiowrite('./sygnal1.wav', y1',fs, 'BitsPerSample', 16);

clear all
%======= odczyt z wav
[y,Fs]=audioread('./sygnal1.wav');
t=0:(1/Fs):1;
f1=700;
f2=70;
A=1;
y1=A*sin(2*pi*f1*t);
alfa1=2;
alfa2=6;
% yt=y1.*exp(-alfa*t);
% sound(yt,Fs)
% figure(1)
% plot(t,yt);
ym=2*A*y1.*sin(2*pi*f2*t);
ym1=sin(2*pi*10*t).*sin(2*pi*1000*t);
ym2=sin(2*pi*10*t).*sin(2*pi*1000*t).*exp(-alfa1*t);
ym3=sin(2*pi*10*t).*sin(2*pi*1000*t).*exp(-alfa2*t);
% figure(2)
 plot(ym2); hold on; plot(ym3);grid

Y=[ym2;ym3];
audiowrite('./sygnal2.wav', Y',Fs,'BitsPerSample' ,16);
[Y1,FS]=audioread('./sygnal2.wav');
figure(1)
%plot(Y1(1,:))
hold on
%plot(Y1(2,:),'r');
% % sound(Y1,Fs);
%z narastaniem na jeden, a z tlumieniem na drugi
ym4=y1.*(1-exp(-alfa2*t));
Y2=[ym2;ym4];
audiowrite('./sygnal3.wav', Y1,Fs,'BitsPerSample',16);
[Y3,FS1]=audioread('./sygnal3.wav');
figure(2)
plot(Y2(2,:))
hold on
plot(Y2(1,:),'r');grid
% sound(Y3,FS1);
