clear all; close all; clc
% GENERACJA SYGNAU
N=8; % liczba probek sygnalu
x=0:N-1; % przykladowe wartosci probek
typBitReverse=1; % 1 lub 2 - wybor algorytmu przestawiania probek (wolna, szybka)
typFFT=2; % 1 lub 2 - wybor wlasciwej petli FFT (wolna, szybka)
xc = x; % kopia sygnalu x


% PRZESTAWIENIE KOLEJNOSCI PROBEK: wersja 1 wolna --
if (typBitReverse==1)
    tic
MSB=log2(N); % liczba bitow numerow probek
 for n=0:N-1; % kolejne probki
 ncopy=n; % stary numer probki (kopia)
 nr=0; % nowy numer probki (inicjalizacja)
 for m=1:MSB % po wszystkich bitach
 if (rem(n,2)==0) % czy jedynka na LSB
   n=n/2; % jesli nie, przesun w prawo
 else 
nr=nr+2^(MSB-m); % dodaj 2^(MSB-m)
 n=(n-1)/2; % odejmij jedynke, przesun w prawo
 end
end
y(nr+1)=x(ncopy+1);% skopiuj we wlasciwe miejsce
 
 end
x = y; % podstaw wynik pod x
toc
end

% PRZESTAWIENIE KOLEJNOSCI PROBEK: wersja 2 szybka (przestawianie w miejscu) 
if (typBitReverse==2)
    tic
a=1;
for b=1:N-1
if (b<a)
T=x(a); x(a)=x(b); x(b)=T;
end
c=N/2;
while (c<a)
a=a-c; c=c/2;
end
a=a+c;
end
toc
end


% WASCIWE FFT - wersja 1 - wolniejsza, bardziej pogladowa -------------------------------------------------------
if (typFFT==1)
    tic
for e = 1 : log2(N) % KOLEJNE ETAPY
SM = 2^(e-1); % szerokosc motylka
LB = N/(2^e); % liczba blokow
LMB = 2^(e-1); % liczba motylkow w bloku
OMB = 2^e; % odleglosc miedzy blokami
W = exp(-j*2*pi/2^e); % podstawa bazy Fouriera
for b = 1 : LB % KOLEJNE BLOKI
for m = 1 : LMB % KOLEJNE MOTYLKI
g = (b-1)*OMB + m; % indeks gornej probki motylka
d = g + SM; % indeks dolnej probki motylka
xgora = x(g); % skopiowanie gornej probki
xdol = x(d)*W^(m-1); % korekta dolnej probki
x(g) = xgora + xdol; % nowa gorna probka: gorna plus dolna po korekcie
x(d) = xgora - xdol; % nowa dolna probka: gorna minus dolna po korekcie
end % koniec petli motylkow
end % koniec petli blokow
end % koniec petli etapow
toc
end

% WASCIWE FFT - wersja 2 - szybsza 
if (typFFT==2)
    tic
for e = 1 : log2(N) % KOLEJNE ETAPY
L = 2^e; % dlugosc blokow DFT, przesuniecie blokow
M = 2^(e-1); % liczba motylkow w bloku, szerokosc kazdego motylka
Wi = 1; % WL l = W 2 0 k = 1 startowa wartosc wsp. bazy w etapie
W = cos(2*pi/L)-j*sin(2*pi/L); % mnoznik bazy Fouriera WL 1 = W2  k1
for m = 1 : M % KOLEJNE MOTYLKI
for g = m : L : N % W KOLEJNYCH BLOKACH
d = g+M; % g  gorny, d  dolny indeks probki motylka
T = x(d)*Wi; % serce FT
x(d) = x(g)-T; % nowa dolna probka: gorna minus serce
x(g) = x(g)+T; % nowa gorna probka: gorna plus serce
end % koniec petli blokow
Wi=Wi*W; % kolejna wartosc bazy Fouriera WL  l = W2  k l
end % koniec petli motylkow
end % koniec petli etapow
toc
end

% POROWNAJ Z MATLABEM --------------------------------------------------------------------------------------------
tic
xc = fft(xc);
toc
blad_real = max(abs(real(x-xc)));
blad_imag = max(abs(imag(x-xc)));

