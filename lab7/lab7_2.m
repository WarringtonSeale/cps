clc
clear all
close all
f1=100;
f2=250;
f3=400;
fs=1000;
A1=1;
A2=0.8;
A3=0.65;
t=0:(1/fs):0.1;
x=A1*sin(2*pi*f1*t)+A2*sin(2*pi*f2*t)+A3*sin(2*pi*f3*t);
%czestotliwosci odciecia
fo1=300;
fo2=200;
wn1=2*fo1/fs;
wn2=2*fo2/fs;
%filtr butter srednioprzepustowy
N=[2,4,8];
[B,A]=butter(N(1,3),[wn2 wn1]);
xf=filter(B,A,x);
%transformata Fouriera
n=length(x);
fft_moc=fft(x(1:n));
moc_wid=fft_moc.*conj(fft_moc)/n;
f=fs*(0:n/2-1)/n;
widmo=sqrt(fft_moc.*conj(fft_moc))/n;
%F=[sygnal,przefiltrowany] W=[sygn,filtrowany]
F(1,:)=f;
W(1,:)=widmo;
n=length(xf);
fft_moc=fft(xf(1:n));
moc_wid=fft_moc.*conj(fft_moc)/n;
f=fs*(0:n/2-1)/n;
widmo=sqrt(fft_moc.*conj(fft_moc))/n;
F(2,:)=f;
W(2,:)=widmo;
b1=fir1(16,[wn2 wn1]);
y_fir=filter(b1,1,x);
%transformata Fouriera
n=length(y_fir);
fft_moc=fft(y_fir(1:n));
moc_wid=fft_moc.*conj(fft_moc)/n;
f=fs*(0:n/2-1)/n;
widmo=sqrt(fft_moc.*conj(fft_moc))/n;
%F=[sygnal,przefiltrowany] W=[sygn,filtrowany]
F(3,:)=f;
W(3,:)=widmo;
figure(1)
subplot(2, 2, 1)
plot(t,x);grid
xlabel('czas [s]');
ylabel('amplituda');
title('sygnal oryginalny');
subplot(2,2,2)
plot(t,xf);grid
xlabel('czas [s]');
ylabel('amplituda');
title({'sygnal filtrowany IIR','fodc1=200Hz fodc2=300Hz'});
figure(2)
subplot(2, 2, 1)
plot(t,x);grid
xlabel('czas [s]');
ylabel('amplituda');
title('sygnal oryginalny');
subplot(2,2,2)
plot(t,y_fir);grid
xlabel('czas [s]');
ylabel('amplituda');
title({'s. filtrowany FIR', 'fodc1=200Hz fodc2=300Hz'});
figure(1)
subplot(2,2,3)
plot(F(1,:),W(1,(1:length(x)/2)));grid
xlabel('czestotliwosc [Hz]');
ylabel('widmo transformaty Fouriera');
title('sygnal oryginalny');
subplot(2,2,4)
plot(F(2,:),W(2,(1:length(xf)/2)));grid
xlabel('czestotliwosc [Hz]');
ylabel('widmo transformaty Fouriera');
title({'s. filtrowany IIR','fodc1=200Hz fodc2=300Hz'});
figure(2)
subplot(2,2,3)
plot(F(1,:),W(1,(1:length(x)/2)));grid
xlabel('czestotliwosc [Hz]');
ylabel('widmo transformaty Fouriera');
title('sygnal oryginalny');
subplot(2,2,4)
plot(F(3,:),W(3,(1:length(y_fir)/2)));grid
xlabel('czestotliwosc [Hz]');
ylabel('widmo transformaty Fouriera');
title({'sygnal filtrowany FIR','fodc1=200Hz fodc2=300Hz'});
figure(3)
subplot(3, 1, 1)
spectrogram(x,'yaxis')
title('Sygnal oryginalny');ylabel('\omega_{norm}[\pi rad/pr]')
subplot(3, 1, 2)
spectrogram(xf,'yaxis')
title('Sygnal filtrowany IIR');ylabel('\omega_{norm}[\pi rad/pr]')
subplot(3, 1, 3)
spectrogram(y_fir,'yaxis')
title('Sygnal filtrowany FIR');ylabel('\omega_{norm}[\pi rad/pr]')
b1=fir1(8,[wn2 wn1]);
figure(4)
subplot(3, 1, 1)
T = 0:(1/fs):1.023;
X = chirp(T,50,1.023,450);
spectrogram(X,256,250,256,1E3,'yaxis')
title('Sygnal oryginalny')
subplot(3, 1, 2)
y_firr=filter(b1,1,X);
spectrogram(y_firr,256,250,256,'yaxis');ylabel('\omega_{norm}[\pi rad/pr]')
title('Sygnal filtrowany FIR')
subplot(3, 1, 3)
y_butter=filter(B,A,X);
spectrogram(y_butter,256,250,256,'yaxis');ylabel('\omega_{norm}[\pi rad/pr]')
title('Sygnal filtrowany IIR')
